#use wml::debian::translation-check translation="156615cc19b61bffcfb93a5ff5e5e300fcbc9492" mindelta="-1"
<define-tag pagetitle>데비안 10 업데이트: 10.8 릴리스</define-tag>
<define-tag release_date>2021-02-06</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>데비안 프로젝트는 데비안 <release> (codename <q><codename></q>) 안정 배포판의 여덟번째 업데이트가 나온 것을 알려드리게 되어 기쁘게 생각합니다.
이 포인트 릴리스는 몇몇 심각한 문제의 조치 및 보안 이슈와 관련된 수정을 주로 담았습니다.
보안 권고는 이미 개별적으로 공개되었고 활용 가능한 곳에서 참조될 수 있습니다.
</p>

<p>포인트 릴리스는 데비안 <release>의 새 버전을 만드는 것이 아니며, 
포함된 일부 패키지만 업데이트 한다는 것을 주의하세요.
이전 버전의 <q><codename></q> 미디어를 버릴 필요 없습니다. 
설치 후, 최신 데비안 미러를 써서 패키지를 현재 버전으로 업그레이드 할 수 있습니다.</p>

<p>security.debian.org의 업데이트를 자주 설치하는 사람들은 패키지를 많이
업데이트하지 않아도 되며, 해당 업데이트는 대부분 포인트 릴리스에 포함되어
있습니다.</p>

<p>새 설치 이미지는 정규 위치(다운로드 페이지, ftp 서버 등)에 곧 공개될 겁니다.
</p>

<p>패키지 관리 시스템이 수많은 데비안 HTTP 미러 중 하나를 가리키게 해서 
기존 설치를 이 개정판으로 업그레이드할 수 있습니다.
포괄적인 미러 서버 목록은 아래에 있습니다:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>기타 버그 고침</h2>

<p>이 버전의 안정 업데이트는 아래 패키지에 몇몇 중요한 수정을 더했습니다:</p>

<table border=0>
<tr><th>패키지</th>               <th>이유</th></tr>
<correction atftp "Fix denial of service issue [CVE-2020-6097]">
<correction base-files "Update /etc/debian_version for the 10.8 point release">
<correction ca-certificates "Update Mozilla CA bundle to 2.40, blacklist expired <q>AddTrust External Root</q>">
<correction cacti "Fix SQL injection issue [CVE-2020-35701] and stored XSS issue">
<correction cairo "Fix mask usage in image-compositor [CVE-2020-35492]">
<correction choose-mirror "Update mirror list">
<correction cjson "Fix infinite loop in cJSON_Minify">
<correction clevis "Fix initramfs creation; clevis-dracut: Trigger initramfs creation upon installation">
<correction cyrus-imapd "Fix version comparison in cron script">
<correction debian-edu-config "Move host keytabs cleanup code out of gosa-modify-host into a standalone script, reducing LDAP calls to a single query">
<correction debian-installer "Use 4.19.0-14 Linux kernel ABI; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-installer-utils "Support partitions on USB UAS devices">
<correction device-tree-compiler "Fix segfault on <q>dtc -I fs /proc/device-tree</q>">
<correction didjvu "Add missing build-dependency on tzdata">
<correction dovecot "Fix crash when searching mailboxes containing malformed MIME messages">
<correction dpdk "New upstream stable release">
<correction edk2 "CryptoPkg/BaseCryptLib: fix NULL dereference [CVE-2019-14584]">
<correction emacs "Don't crash with OpenPGP User IDs with no e-mail address">
<correction fcitx "Fix input method support in Flatpaks">
<correction file "Increase name recursion depth to 50 by default">
<correction geoclue-2.0 "Check the maximum allowed accuracy level even for system applications; make the Mozilla API key configurable and use a Debian-specific key by default; fix display of the usage indicator">
<correction gnutls28 "Fix test suite error caused by expired certificate">
<correction grub2 "When upgrading grub-pc noninteractively, bail out if grub-install fails; explicitly check whether the target device exists before running grub-install; grub-install: Add backup and restore; don't call grub-install on fresh install of grub-pc">
<correction highlight.js "Fix prototype pollution [CVE-2020-26237]">
<correction intel-microcode "Update various microcode">
<correction iproute2 "Fix bugs in JSON output; fix race condition that DOSes the system when using ip netns add at boot">
<correction irssi-plugin-xmpp "Do not trigger the irssi core connect timeout prematurely, thus fixing STARTTLS connections">
<correction libdatetime-timezone-perl "Update for new tzdata version">
<correction libdbd-csv-perl "Fix test failure with libdbi-perl 1.642-1+deb10u2">
<correction libdbi-perl "Security fix [CVE-2014-10402]">
<correction libmaxminddb "Fix heap-based buffer over-read [CVE-2020-28241]">
<correction lttng-modules "Fix build on kernel versions &gt;= 4.19.0-10">
<correction m2crypto "Fix compatibility with OpenSSL 1.1.1i and newer">
<correction mini-buildd "builder.py: sbuild call: set '--no-arch-all' explicitly">
<correction net-snmp "snmpd: Add cacheTime and execType flags to EXTEND-MIB">
<correction node-ini "Do not allow invalid hazardous string as section name [CVE-2020-7788]">
<correction node-y18n "Fix prototype pollution issue [CVE-2020-7774]">
<correction nvidia-graphics-drivers "New upstream release; fix possible denial of service and information disclosure [CVE-2021-1056]">
<correction nvidia-graphics-drivers-legacy-390xx "New upstream release; fix possible denial of service and information disclosure [CVE-2021-1056]">
<correction pdns "Security fixes [CVE-2019-10203 CVE-2020-17482]">
<correction pepperflashplugin-nonfree "Turn into a dummy package taking care of removing the previously installed plugin (no longer functional nor supported)">
<correction pngcheck "Fix buffer overflow [CVE-2020-27818]">
<correction postgresql-11 "New upstream stable release; security fixes [CVE-2020-25694 CVE-2020-25695 CVE-2020-25696]">
<correction postsrsd "Ensure timestamp tags aren't too long before trying to decode them [CVE-2020-35573]">
<correction python-bottle "Stop allowing <q>;</q> as a query-string separator [CVE-2020-28473]">
<correction python-certbot "Automatically use ACMEv2 API for renewals, to avoid issues with ACMEv1 API removal">
<correction qxmpp "Fix potential SEGFAULT on connection error">
<correction silx "python(3)-silx: Add dependency on python(3)-scipy">
<correction slirp "Fix buffer overflows [CVE-2020-7039 CVE-2020-8608]">
<correction steam "New upstream release">
<correction systemd "journal: do not trigger assertion when journal_file_close() is passed NULL">
<correction tang "Avoid race condition between keygen and update">
<correction tzdata "New upstream release; update included timezone data">
<correction unzip "Apply further fixes for CVE-2019-13232">
<correction wireshark "Fix various crashes, infinite loops and memory leaks [CVE-2019-16319 CVE-2019-19553 CVE-2020-11647 CVE-2020-13164 CVE-2020-15466 CVE-2020-25862 CVE-2020-25863 CVE-2020-26418 CVE-2020-26421 CVE-2020-26575 CVE-2020-28030 CVE-2020-7045 CVE-2020-9428 CVE-2020-9430 CVE-2020-9431]">
</table>


<h2>보안 업데이트</h2>


<p>이 개정판은 아래의 보안 업데이트를 안정 릴리스에 추가합니다.
보안팀은 각 업데이트에 대해서 이미 권고사항을 공개했습니다:</p>

<table border=0>
<tr><th>권고 ID</th>  <th>패키지</th></tr>
<dsa 2020 4797 webkit2gtk>
<dsa 2020 4801 brotli>
<dsa 2020 4802 thunderbird>
<dsa 2020 4803 xorg-server>
<dsa 2020 4804 xen>
<dsa 2020 4805 trafficserver>
<dsa 2020 4806 minidlna>
<dsa 2020 4807 openssl>
<dsa 2020 4808 apt>
<dsa 2020 4809 python-apt>
<dsa 2020 4810 lxml>
<dsa 2020 4811 libxstream-java>
<dsa 2020 4812 xen>
<dsa 2020 4813 firefox-esr>
<dsa 2020 4814 xerces-c>
<dsa 2020 4815 thunderbird>
<dsa 2020 4816 mediawiki>
<dsa 2020 4817 php-pear>
<dsa 2020 4818 sympa>
<dsa 2020 4819 kitty>
<dsa 2020 4820 horizon>
<dsa 2020 4821 roundcube>
<dsa 2021 4822 p11-kit>
<dsa 2021 4823 influxdb>
<dsa 2021 4824 chromium>
<dsa 2021 4825 dovecot>
<dsa 2021 4827 firefox-esr>
<dsa 2021 4828 libxstream-java>
<dsa 2021 4829 coturn>
<dsa 2021 4830 flatpak>
<dsa 2021 4831 ruby-redcarpet>
<dsa 2021 4832 chromium>
<dsa 2021 4833 gst-plugins-bad1.0>
<dsa 2021 4834 vlc>
<dsa 2021 4835 tomcat9>
<dsa 2021 4837 salt>
<dsa 2021 4838 mutt>
<dsa 2021 4839 sudo>
<dsa 2021 4840 firefox-esr>
<dsa 2021 4841 slurm-llnl>
<dsa 2021 4843 linux-latest>
<dsa 2021 4843 linux-signed-amd64>
<dsa 2021 4843 linux-signed-arm64>
<dsa 2021 4843 linux-signed-i386>
<dsa 2021 4843 linux>
</table>


<h2>없앤 패키지</h2>

<p>다음 패키지는 우리 제어를 넘는 환경으로 인해 없어졌습니다:</p>

<table border=0>
<tr><th>패키지</th>               <th>이유</th></tr>
<correction compactheader "Incompatible with current Thunderbird versions">

</table>

<h2>데비안 설치관리자</h2>
<p>설치관리자는 포인트 릴리스에서 안정 릴리스에서 병합된 수정을 포함하도록 업데이트되었습니다.</p>

<h2>URL</h2>

<p>이 개정판에서 바뀐 패키지 전체 목록:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>현재 안정 배포:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>안정 배포 정보(release notes, 정오표 등.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>보안 알림 및 정보:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>데비안은</h2>

<p>>데비안 프로젝트는 완전한 자유 운영체제인 데비안을 제작하기 위해 자신의 시간과 노력을 자원하는 자유 소프트웨어 개발자 모임입니다.</p>

<h2>연락 정보</h2>

<p>더 많은 정보를 위해 <a href="$(HOME)/">https://www.debian.org/</a>에 있는 데비안 웹 페이지를 방문하거나, 
&lt;press@debian.org&gt;으로 이메일을 보내세요.
또는 &lt;debian-release@lists.debian.org&gt;로 보내서 안정 릴리스 팀으로 연락하세요.</p>


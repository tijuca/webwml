#use wml::debian::template title="Autobuilder network"
#use wml::debian::translation-check translation="63e5091d45458638e03a89dee1560f547b0cf0fc" maintainer="Giovanni Mascellani"

<p>La rete di buildd è un sottoprogetto di Debian che gestisce
la compilazione dei pacchetti per tutte le architetture che
<a href="$(HOME)/ports/">Debian al momento supporta</a>. Questa rete è formata
da svariate macchine (i nodi buildd) che utilizzano uno specifico pacchetto
software, chiamato <em>buildd</em>, per prendere i pacchetti dall'archivio
Debian e ricompilarli per l'architettura richiesta.</p>

<h2>Come mai è necessaria la rete di buildd?</h2>

<p>La distribuzione Debian supporta <a href="$(HOME)/ports/">un buon numero di
architetture</a>, ma i mantenitori dei pacchetti in genere compilano i binari
solo per una sola architettura a cui hanno accesso (di solito i386 o amd64).
Le altre compilazioni sono eseguite automaticamente assicurando che un
pacchetto sia creato in una sola volta. Eventuali errori sono tracciati nel
database di autobuilder.</p>

<p>Quando fu iniziata Debian/m68k (il primo port non Intel) gli sviluppatori
di questo port controllavano l'arrivo di nuove versioni dei pacchetti e
dovevano ricompilarle se ne volevano stare allineati con la distribuzione
per Intel. Tutto questo veniva
fatto manualmente: gli sviluppatori controllavano sulla mailing list degli
upload la presenza di nuovi pacchetti e ne prendevano alcuni per compilarli.
Annunciando su una mailing list cosa si stava per fare si evitava che un
pacchetto venisse compilato due volte. È evidente che però questo metodo era
facilmente soggetto ad errori ed estremamente costoso in termini di tempo, ma è
stato per lungo tempo l'unico modo di mantenere le distribuzioni non i386.</p>

<p>Il demone di compilazione rende automatica la maggior parte di questo
processo. Consiste di una serie di script (scritti in Perl e Python), che sono
stati modificati molte volte nel corso del tempo in modo da aiutare i porter
in molti compiti. Sono finalmente diventati un sistema che è capace di
mantenere quasi automaticamente le varie distribuzioni Debian aggiornate.
Gli aggiornamenti per la sicurezza sono compilati sulle stesso insieme di
macchine in modo da assicurare che siano tempestivamente disponibili.
</p>

<h2>Come funziona buildd?</h2>

<p><em>Buildd</em> è il nome che di solito si dà ai programmi utilizzati
della rete, ma in realtà è però diviso in parti diverse:</p>

<dl>

<dt>wanna-build</dt>
<dd>
un tool che coordina la (ri)compilazione dei pacchetti attraverso un database
che mantiene la lista dei pacchetti e del loro stato. C'è un database centrale
per ogni architettura che memorizza lo stato dei pacchetti, la loro versione
e qualche altra informazione. È alimentato con i file Sources e Packages
recuperati dai vari archivi dei pacchetti Debian (per esempio ftp-master e
security-master).
</dd>


<dt><a href="https://packages.debian.org/buildd">buildd</a></dt>
<dd>
un demone che controlla periodicamente il database mantenuto da
<em>wanna-build</em> e chiama <em>sbuild</em> per compilare i pacchetti. Una
volta che la compilazione viene approvata da un amministratore, carica il
pacchetto nell'archivio appropriato.
</dd>

<dt><a href="https://packages.debian.org/sbuild">sbuild</a></dt>
<dd>
è responsabile dell'effettiva compilazione dei pacchetti in chroot isolate.
Si assicura che tutte le dipendenze di compilazione siano soddisfatte, poi,
utilizzando comandi standard di Debian, avvia la compilazione. I log di
compilazione sono aggiunti al <a href="https://buildd.debian.org">database
dei log di compilazione</a>.
</dd>

</dl>

<p>Tutte queste parti <a href="operation">operano</a> insieme per far funzionare
la rete di buildd.</p>

<h2>Cosa deve fare uno sviluppatore Debian?</h2>

<p>In realtà lo sviluppatore Debian medio non deve esplicitamente usare
la rete di buildd. Quando carica un pacchetto nell'archivio (binari
compilati per una certa architettura), questo sarà aggiunto al database di ogni
architettura (nello stato <em>Needs-Build</em>, ossia "compilazione
necessaria"). Le macchine di compilazione interrogheranno il database chiedendo
quali pacchetti sono in quello stato e prenderanno continuamente pacchetti dalla
lista. I criteri di priorità della lista sono lo stato della precedente
compilazione (<em>Out-Of-Date</em>, "non aggiornato", oppure <em>Uncompiled</em>,
"non compilato"), la priorità del pacchetto, la sua sezione ed il suo nome.
Inoltre, per evitare che alcuni pacchetti rimangano sempre in fondo alla coda
e non vengano mai compilati, le priorità sono aggiustate dinamicamente
man mano che il tempo di attesa aumenta.</p>

<p>Se la compilazione ha successo su tutte le architetture, il mantenitore
non avrà bisogno di fare niente. Tutti i pacchetti binari saranno caricati
nell'archivio corrispondente. Se la compilazione non finisce
con successo, il pacchetto entrerà in uno stato speciale (<em>Build-Attempted</em>,
"compilazione tentata", per compilazioni fallite che non sono state ancora
revisionate dagli amministratori, <em>Failed</em>, "fallito", una volta
che il fallimento è stato revisionato e riportato come bug, oppure
<em>Dep-Wait</em>, "aspetta le dipendenze", se presentano dipendenze di
compilazione non disponibili). Gli amministratori
della rete di buildd controlleranno i pacchetti che non sono stati compilati
con successo e ne daranno comunicazione al mantenitore, generalmente aprendo un
bug nel Bug Tracking System.</p>

<p>A volte un pacchetto impiega molto tempo per essere compilato per una certa
architettura, e questo gli impedisce di entrare in
<a href="$(HOME)/devel/testing">testing</a>. Se un pacchetto sta bloccando
una transizione tipicamente le priorità di compilazione vengono aggiustate
su richiesta del Team di Rilascio. Altre richieste di questo tipo non
saranno accettate, dal momento che il pacchetto guadagna automaticamente
priorità man mano che rimane nella coda.</p>

<p>Puoi controllare lo stato di ogni tentativo fatto da buildd per compilare i
pacchetti che appartengono ad ogni mantenitore controllando i
<a href="https://buildd.debian.org/status/">log di buildd</a>. Questi log
possono essere raggiunti anche dal Riassunto dei Pacchetti di un
Mantenitore (Packages' Maintainer Overview).</p>

<p>Per maggiori informazioni sui diversi stati dei pacchetti puoi leggere
<a href="wanna-build-states">"stati di wanna-build"</a>.</p>

<h2>Dove posso trovare altre informazioni?</h2>

<p>Ovviamente, il miglior modo di capire come funziona la rete i buildd è
consultare i codici sorgente e la documentazione disponibile. Inoltre, la
sezione <a href="$(HOME)/doc/manuals/developers-reference/pkgs.html#porting">\
Porting and being ported</a> della <a
href="$(HOME)/doc/manuals/developers-reference/">Debian Developers Reference</a>
contiene altre informazioni su come funziona, nonché informazioni sui
<a href="$(HOME)/doc/manuals/developers-reference/tools.html#tools-builders">\
compilatori di pacchetti</a> e
<a href="$(HOME)/doc/manuals/developers-reference/tools.html#tools-porting">\
tool per il porting</a> che sono utilizzati nel processo di costruzione e
mantenimento della rete di buildd.</p>

<p>Sono disponibili <a href="https://buildd.debian.org/stats/">alcune statistiche
sulla rete di buildd</a>.</p>

<h2>Come faccio a installare il mio nodo buildd personale?</h2>

<p>Ci sono molte ragioni per cui uno sviluppatore (o un utente) potrebbe voler
metter su e far funzionare un sistema buildd:</p>

<ul>
<li>Per aiutare nel port su una certa architettura (quando sono necessari nodi
buildd).</li>
<li>Per valutare l'effetto di una certa patch o ottimizzazione del compilatore
su un grande numero di pacchetti.</li>
<li>Per utilizzare tool che analizzano pacchetti cercando errori comuni e che
lavorano sui pacchetti compilati, operazione che può anche essere necessaria
per fare analisi sul codice sorgente, per esempio come work-around per pacchetti
che usano <tt>dpatch</tt>.</li>
</ul>

<p>Qui puoi trovare ulteriori informazioni su come
<a href="https://wiki.debian.org/BuilddSetup">installare un nodo buildd</a>.</p>

<h2>Contattare gli amministratori dei nodi buildd</h2>

<p>Gli amministratori responsabili per i nodi buildd di una particolare
architetture possono essere raggiunti all'indirizzo
<email arch@buildd.debian.org>, per esempio <email i386@buildd.debian.org>.</p>

<hrline />

<p><small>Questa introduzione alla rete di buildd è stata scritta mettendo
insieme pezzetti appartenenti a Roman Hodek, Christian T. Steigies,
Wouter Verhelst, Andreas Barth, Francesco Paolo Lovergine,
Javier Fernández-Sanguino e Philipp Kern.</small></p>


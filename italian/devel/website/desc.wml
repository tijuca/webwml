#use wml::debian::template title="Com'è realizzato www.debian.org" MAINPAGE="true"
#use wml::debian::translation-check translation="835e2e2ec1141439d03f50e38dba3a48eaaf6e81" maintainer="Giuseppe Sacco"
#use wml::debian::toc

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#look">Look &amp; Feel</a></li>
<li><a href="#sources">Sorgenti</a></li>
<li><a href="#scripts">Script</a></li>
<li><a href="#generate">Generare il sito web</a></li>
<li><a href="#help">Come aiutare</a></li>
<li><a href="#faq">Come non aiutare... (FAQ)</a></li>
</ul>

<h2><a id="look">Look &amp; Feel</a></h2>

<p>Il sito Debian è una raccolta di directory e file in
<code>/org/www.debian.org/www</code> su <em>www-master.debian.org</em>.
Molte pagine sono file HTML statici. Non contengono elementi dinamici
come script CGI o PHP poiché il sito viene replicato sui mirror.
</p>

<p>
Il sito web Debian usa il Website Meta Language
(<a href="https://packages.debian.org/unstable/web/wml">WML</a>) per generare pagine HTML,
incluse intestazioni e più di pagina, <em>heading</em>, indici, eccetera.
Anche se un file <code>.wml</code> può sembrare, ad un primo sguardo, simile ad uno HTML,
HTML è solo uno dei tipi di informazione aggiuntiva che si può usare in WML. Si può anche
includere codice Perl e quindi fare quasi qualsiasi cosa.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Le nostre pagine web soddisfano i requisiti dello standard <a href="http://www.w3.org/TR/html4/">HTML 4.01 Strict</a>.</p>
</aside>

<p>
Dopo che WML completa a i passaggi dei vari filtri sui un file, il prodotto
finale è vero HTML. Notare che nonostante WML controlli, e qualche volta
magicamente corregga, la validità di base del codice HTML, sarebbe
meglio installare uno strumento come
<a href="https://packages.debian.org/unstable/web/weblint">weblint</a> 
e/o
<a href="https://packages.debian.org/unstable/web/tidy">tidy</a> 
per un controllo minimo della sintassi.
</p>

<p>
Chiunque contribuisca con regolarità al sito web Debian dovrebbe installare
WML per controllare il codice ed essere sicuro che le pagine HTML
finali siano corrette. Se si utilizza Debian, basta installare il
pacchetto <code>wml</code>.
Per maggiori informazioni controllare la pagina <a href="using_wml">using WML</a>.
</p>

<h2><a id="sources">Sorgenti</a></h2>

<p>
Usiamo Git per memorizzare i sorgenti del sito web Debian. Il sistema di
controllo delle versioni ci permette di tenere traccia di tuttte le modifiche
e possiamo controllare chi ha fatto cosa e quando, anche perché. Git offre
un modo sicuro per controllare la modifica in contemporanea di file sorgenti
da parte di più autori — un compito cruciale per il gruppo web di Debian,
perché vi sono parecchi membri.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="using_git">Leggere di più su Git</a></button></p>

<p>
Ecco alcune informazioni preliminari su come sono strutturati i sorgenti:
</p>

<ul>
  <li>La directory radice del repository Git (<code>webwml</code>) contiene
directory nominate in accordo con la lingua del loro contenuto,
due Makefile e alcuni script. I nomi delle directory per le pagine tradotte
devono essere in inglese e usare solo lettere minuscole, per esempio
<code>german</code> e non <code>Deutsch</code>.</li>

  <li>Il file <code>Makefile.common</code> è particolarmente importante
poiché contiene alcune regole comuni che sono richiamate includendo questo
file in altri Makefile.</li>

  <li>Ogni sottodirectory delle varie lingue contiene un Makefile, vari 
file sorgente <code>.wml</code> e ulteriori sottodirectory.
I nome di tutti i file e directory seguono certi modelli in modo che ogni link
funzioni per tutte le pagine tradotte.
Alcune directory contengono anche file di configurazione
<code>.wmlrc</code> con comandi e impostazioni aggiuntive per WML.</li>

  <li>La directory <code>webwml/english/template</code> contiene file WML speciali che
funzionano come modelli. Possono essere richiamati da altri file con il comando
<code>#use</code>.</li>
</ul>

<p>
Nota: per accertarsi che le modifiche ai modelli siano propagate ai file
che li includono, i file hanno delle dipendenze impostate nel Makefile.
La stragrande maggioranza dei file usa il modello <code>template</code>,
la dipendenza generica, e quindi contengno questa prima linea:
</p>
 
<p>
<code>#use wml::debian::template</code>
</p>
 
<p>
Ci sono eccezioni a questa regola, naturalmente.
</p>

<h2><a id="scripts">Script</a></h2>

<p>
Gli script sono, per la maggior parte, scritti in shell o Perl.
Alcuni di loro sono a sé stanti, altri sono integrati in
file sorgente WML.
</p>

<ul>
  <li><a href="https://salsa.debian.org/webmaster-team/cron.git">webmaster-team/cron</a>: 
Questo repository Git contiene tutti gli script usati per aggiornare il sito web Debian,
vale a dire i sorgenti degli script che ricostruiscono <code>www-master</code>.</li>
  <li><a href="https://salsa.debian.org/webmaster-team/packages">webmaster-team/packages</a>: 
Questo repository Git contiene i sorgenti degli script che ricostruiscono
<code>packages.debian.org</code>.</li>
</ul>

<h2><a id="generate">Generare il sito web</a></h2>

<p>
WML, modelli e script shell o Perl sono tutti gli ingredienti necessari a generare il sito web Debian:
</p>

<ul>
  <li>Per lo più è generato usando WML (dal <a href="$(DEVEL)/website/using_git">repository Git</a>).</li>
  <li>La documentazione è generata con DocBook XML (<a href="$(DOC)/vcs">repository Git <q>ddp</q></a>) 
oppure con <a href="#scripts">script cron</a> dei corrispondenti pacchetti Debian.</li> 
  <li>Alcune parti del sito web sono generate con script che usano altre fonti, per
esempio, le pagine per (dis)iscriversi alle liste di messaggi.</li>
</ul>

<p>
Sei volte al giorno viene fatto un aggiornamento automatico a partire dal repository Git e altre sorgenti.
Oltre questo, vengono eseguiti i seguenti controlli su tutto il sito web:
</p>

<ul>
  <li><a href="https://www-master.debian.org/build-logs/urlcheck/">URL check</a>
  <li><a href="https://www-master.debian.org/build-logs/validate/">wdg-html-validator</a>
  <li><a href="https://www-master.debian.org/build-logs/tidy/">tidy</a>
</ul>

<p>
Gli ultimi log della costruzione del sito web possono essere
trovati in <url "https://www-master.debian.org/build-logs/">.
</p>

<p>Se si vuole contribuire al sito, <strong>non</strong> modificare direttamente
i file nella directory <code>www/</code> o aggiungere nuovi elementi. Invece,
contattare prima i <a href="mailto:webmaster@debian.org">webmaster</a>.
</p>

<aside>
<p><span class="fas fa-cogs fa-3x"></span>Una nota più tecnica:
tutti i file e le directory sono di proprietà del gruppo <code>debwww</code> che ha il permesso
in scrittura. In questo modo tutto il gruppo che si occupa del web può modificarne il
contenuto. Il modo <code>2775</code> sulle directory significa che qualsiasi file creato
all'interno erediterà il gruppo (<code>debwww</code>).
Ci si aspetta dai membri del gruppo che impostino <code>umask 002</code> in modo che i
file siano creati con i permessi di gruppo corretti.</p>
</aside>

<h2><a id="help">Come aiutare</a></h2>

<p>
Invitiamo tutti ad aiutare con il sito web Debian. Se si hanno
informazioni importanti su Debian, che si pensa manchino,
<a href="mailto:debian-www@lists.debian.org">contattaci</a> — faremo in modo che siano incluse.
Inoltre, dare un'occhiata ai summenzionati
<a href="https://www-master.debian.org/build-logs/">log della costruzione</a> per vedere se si
quanche suggerimento per risolvere i problemi.
</p>

<p>
Stiamo anche cercando persone che possano aiutare con l'aspetto
(immagini, layout, ecc.). Se si parla bene l'inglese sarebbe
utile considerare di rileggere le nostre pagine e
<a href="mailto:debian-www@lists.debian.org">segnalare</a> errori. 
Se si parlano altre lingue si può aiutare a tradurre le pagine
esistenti o risolvere problemi in quelle già tradotte. In
entrambi i casi, dare uno sguardo all'elenco dei
<a href="translation_coordinators">coordinatori delle traduzioni</a> e
entrare in contatto con la persona responsabile. Per maggiori
informazioni controllare la nostra
<a href="translating">pagina per i traduttori</a>.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="todo">Leggere l'elenco delle cose da fare</a></button></p>

<aside class="light">
  <span class="fa fa-question fa-5x"></span>
</aside>

<h2><a id="faq">Come non aiutare... (FAQ)</a></h2>

<p>
<strong>[D] Vorrei aggiungere questa <em>bella caratteristica</em> sul sito web Debian website, posso?</strong>
</p>

<p>
[R] No. Vogliamo che www.debian.org si più accessibile possibile, quindi
</p>

<ul>
    <li>nessuna "estensione" specifica ad un solo browser,</li>
    <li>nessuna dipendenza dalle sole immagini. Le immagini possono essere utilizzate per
        chiarire ma l'informazione in www.debian.org deve rimanere accessibile ai
        browser testuali, come lynx.</li>
</ul>

<p>
<strong>[D] Ho questa idea simpatica che vorrei proporre. Potete abilitare <em>X</em> o <em>Y</em>
su www.debian.org, per favore?</strong>
</p>

<p>
[R] No. Vogliamo rendere la vita facile agli amministratori dei mirror di www.debian.org,
quindi nessuna caratteristica su HTTPD, grazie.
No, neppure SSI (Server Side Includes). Una eccezione è stata fatta per
la negoziazione dei contenuti, perché è l'unico modo robusto per servire
più lingue.
</p>

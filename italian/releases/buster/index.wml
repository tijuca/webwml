#use wml::debian::template title="Informazioni sul rilascio di Debian &ldquo;buster&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="6bafc9758f46d9e4fe5b48e5d85484d9f3dc8e3e" maintainer="Luca Monducci"


<p>Debian GNU/Linux <current_release_buster> è stata rilasciata il
<a href="$(HOME)/News/<current_release_newsurl_buster/>">
<current_release_date_buster></a>.
<ifneq "10.0" "<current_release>"
	"Il rilascio iniziale di Debian 10.0 fu fatto il <:=spokendate('2019-07-06'):>."
/>
Questo rilascio contiene importanti cambiamenti descritti
nel <a href="$(HOME)/News/2019/20190706">comunicato stampa</a> e
nelle <a href="releasenotes">Note di rilascio</a>.</p>

<p><strong>Debian 10 è stata sostituita da
<a href="../bullseye/">Debian 11 (<q>bullseye</q>)</a>.
Gli aggiornamenti per la sicurezza sono stati interrotti
dal <:=spokendate('2022-06-30'):>.
</strong></p>

<p><strong>Buster beneficia anche del Supporto a Lungo Termine (LTS Long
Term Support) fino a Giugno 2024. Tale supporto è limitato alle architetture
i386, amd64, armel e armhf; tutte le altre architetture non hanno supporto.
Per ulteriori informazioni fare riferimento alla <a
href="https://wiki.debian.org/LTS">sezione LTS del Wiki Debian</a>.
</strong></p>

<p>Per ottenere e installare Debian, si veda la pagina
con le <a href="debian-installer/">informazioni sull'installazione</a> e
la <a href="installmanual">Guida all'installazione</a>. Per aggiornare
da un precedente rilascio di Debian, consultare le
<a href="releasenotes">Note di rilascio</a>.</p>

### Activate the following when LTS period starts.
#<p>Architectures supported during Long Term Support:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>In questo rilascio sono supportate le seguenti architetture:</p>
# <p>Computer architectures supported at initial release of buster:</p> ### Use this line when LTS starts, instead of the one above.

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Nonstante la nostra volontà, questo rilascio potrebbe avere problemi,
anche se è dichiarato <em>stable</em>. Esiste un <a href="errata">elenco
dei principali problemi conosciuti</a>, ed è possibile <a
href="../reportingbugs">segnalare altri problemi</a>.</p>

<p>Infine, ma non meno importante, è presente un elenco di
<a href="credits">persone da ringraziare</a> per aver permesso questo
rilascio.</p>

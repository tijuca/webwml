#use wml::debian::translation-check translation="b6a020c494e8e886f401ab63178faa0db93e39da" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 11 aktualisiert: 11.7 veröffentlicht</define-tag>

<define-tag release_date>2023-04-29</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die siebte Aktualisierung seiner Stable-Distribution
Debian <release> (Codename <q><codename></q>) ankündigen zu dürfen. Diese
Zwischenveröffentlichung behebt hauptsächlich Sicherheitslücken der Stable-Veröffentlichung
sowie einige ernste Probleme. Es sind bereits separate Sicherheitsankündigungen
veröffentlicht worden, auf die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version von
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete auffrischt.
Es gibt keinen Grund, <q><codename></q>-Medien zu entsorgen, da deren Pakete
auch nach der Installation durch einen aktualisierten Debian-Spiegelserver auf
den neuesten Stand gebracht werden können.
</p>

<p>Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht viele
Pakete auf den neuesten Stand bringen müssen. Die meisten dieser Aktualisierungen sind
in dieser Revision enthalten.</p>

<p>Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.</p>

<p>Vorhandene Installationen können auf diese Revision angehoben werden, indem
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian verwiesen
wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerbehebungen</h2>

<p>Diese Stable-Aktualisierung fügt den folgenden Paketen einige wichtige Korrekturen hinzu:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction akregator "Gültigkeitsprüfungen überarbeitet, außerdem die Löschung von Feeds und Verzeichnissen">
<correction apache2 "apache2-doc.conf nicht automatisch aktivieren; Regressionen in http2 sowie mod_rewrite, die in 2.4.56 eingebracht wurden, entfernt">
<correction at-spi2-core "Zeitüberschreitung fürs Stoppen auf fünf Sekunden gesetzt, sodass das System beim Herunterfahren nicht unnötig aufgehalten wird">
<correction avahi "Lokale Dienstblockade abgestellt [CVE-2021-3468]">
<correction base-files "Aktualisierung auf die 11.7-Zwischenveröffentlichung">
<correction c-ares "Stapelüberlauf und Dienstblockade unterbunden [CVE-2022-4904]">
<correction clamav "Neue stabile Version der Originalautoren; mögliche Anfälligkeit für Codeausführung aus der Ferne im HFS+-Dateiauswerter behoben [CVE-2023-20032], potenzielles Informationsleck im DMG-Dateiauswerter gestopft [CVE-2023-20052]">
<correction command-not-found "Neue non-free-firmware-Komponente hinzugefügt, um Probleme beim Upgrade auf Bookworm zu beheben">
<correction containerd "Dienstblockade behoben [CVE-2023-25153]; mögliche Privilegieneskalation durch unsachgemäßes Anlegen von zusätzlichen Gruppen abgestellt [CVE-2023-25173]">
<correction crun "Fähigkeiteneskalation durch Container, die fälschlicherweise mit nicht-leeren Vorgabeberechtigungen gestartet werden, abgestellt [CVE-2022-27650]">
<correction cwltool "Fehlende Abhängigkeit von python3-distutils hinzugefügt">
<correction debian-archive-keyring "Bookworm-Schlüssel hinzugefügt; Stretch-Schlüssel in den »removed«-Schlüsselbund verschoben">
<correction debian-installer "Linux-Kernel-ABI auf 5.10.0-22 angehoben; Neukompilierung gegen proposed-updates">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction debian-ports-archive-keyring "Die 2023 ablaufende Gültigkeit des Signierschlüssels um ein Jahr verlängert; Signierschlüssel für 2024 hinzugefügt; 2022er Signierschlüssel in den »removed«-Schlüsselbund verschoben">
<correction dpdk "Neue stabile Version der Originalautoren">
<correction duktape "Absturzprobleme behoben [CVE-2021-46322]">
<correction e2tools "Kompilierungsfehlschlag durch Hinzufügen einer Kopmpilier-Abhängigkeit von e2fsprogs behoben">
<correction erlang "Anfälligkeit für Umgehung der Client-Authentifizierung behoben [CVE-2022-37026]; Optimierung -O1 für armel verwenden, weil erl bei -O2 auf bestimmten Plattformen wie Marvell mit Speicherzugriffsfehler abstürzt">
<correction exiv2 "Sicherheitskorrekturen [CVE-2021-29458 CVE-2021-29463 CVE-2021-29464 CVE-2021-29470 CVE-2021-29473 CVE-2021-29623 CVE-2021-32815 CVE-2021-34334 CVE-2021-34335 CVE-2021-3482 CVE-2021-37615 CVE-2021-37616 CVE-2021-37618 CVE-2021-37619 CVE-2021-37620 CVE-2021-37621 CVE-2021-37622 CVE-2021-37623]">
<correction flask-security "Anfälligkeit für offene Weiterleitungen behoben [CVE-2021-23385]">
<correction flatpak "Neue stabile Version der Originalautoren; Sonderzeichen bei der Anzeige der Berechtigungen und Metadaten maskieren [CVE-2023-28101]; Kopieren/Einfügen via TIOCLINUX-ioctl nicht erlauben, wenn es in einer virtuellen Linux-Konsole läuft [CVE-2023-28100]">
<correction galera-3 "Neue stabile Version der Originalautoren">
<correction ghostscript "Pfad für PostScript-Helferdatei in ps2epsi korrigiert">
<correction glibc "Speicherleck in den Funktionen der printf-Familie bei langen Multibyte-Zeichenketten behoben; Absturz in der printf-Familie durch Breite-/präzisionsabhängige Zuweisungen behoben; Speicherzugriffsfehler in printf beim Umgang mit Tausendertrennzeichen behoben; Überlauf in der AVX2-Implementierung von wcsnlen beim Überschreiten von Seiten behoben">
<correction golang-github-containers-common "Auswertung der DBUS_SESSION_BUS_ADDRESS überarbeitet">
<correction golang-github-containers-psgo "Prozess-Benutzer-Namensraum nicht betreten [CVE-2022-1227]">
<correction golang-github-containers-storage "Bisherige interne Funktionen allgemein zugänglich gemacht, was nötig war, um CVE-2022-1227 in anderen Paketen zu beheben">
<correction golang-github-prometheus-exporter-toolkit "Tests korrigiert, um Race Condition zu vermeiden; Authentifizierungs-Cache-Vergiftung beseitigt [CVE-2022-46146]">
<correction grep "Falsche Übereinstimmungen abgestellt, die auftraten, wenn das letzte von mehreren Mustern einen Rückverweis enthält">
<correction gtk+3.0 "Wayland + EGL auf nur-GLES-Plattformen überarbeitet">
<correction guix "Kompilierungsfehlschlag, der durch abgelaufene Schlüssel in der Test-Suite verursacht wurde, behoben">
<correction intel-microcode "Neue fehlerkorrigierende Veröffentlichung der Originalautoren">
<correction isc-dhcp "Umgang mit Lebensdauer von IPv6-Adressen verbessert">
<correction jersey1 "Kompilierungsfehlschlag mit libjettison-java 1.5.3 behoben">
<correction joblib "Anfälligkeit für eigenmächtige Codeausführung behoben [CVE-2022-21797]">
<correction lemonldap-ng "Anfälligkeit für Umgehung der URL-Validierung behoben; 2FA-Problem bei Verwendung des AuthBasic-Handlers behoben [CVE-2023-28862]">
<correction libapache2-mod-auth-openidc "Anfälligkeit für offene Weiterleitungen behoben [CVE-2022-23527]">
<correction libapreq2 "Pufferüberlauf behoben [CVE-2022-22728]">
<correction libdatetime-timezone-perl "Enthaltene Daten aktualisiert">
<correction libexplain "Kompatibilität mit neueren Linux-Kernelversionen verbessert - Linux 5.11 hat kein if_frad.h mehr, termiox gibt es seit Kernel 5.12 nicht mehr">
<correction libgit2 "SSH-Schlüsselüberprüfung standardmäßig aktiviert [CVE-2023-22742]">
<correction libpod "Anfälligkeit für Privilegieneskalation behoben [CVE-2022-1227]; Fähigkeiteneskalation durch Container, die fälschlicherweise mit nicht-leeren Vorgabeberechtigungen gestartet wurden, behoben [CVE-2022-27649]; Verarbeitung der DBUS_SESSION_BUS_ADDRESS überarbeitet">
<correction libreoffice "Kroatiens Standardwährung auf Euro umgestellt; leeren -Djava.class.path= vermeiden [CVE-2022-38745]">
<correction libvirt "Probleme behoben, die mit Container-Neustarts zu tun haben; Testfehlschläge behoben, die zusammen mit neueren Xen-Versionen auftraten">
<correction libxpm "Anfälligkeit für Endlosschleifen behoben [CVE-2022-44617 CVE-2022-46285]; Doppel-free im Code für Fehlerbehandlung behoben; <q>Komprimierbefehle hängen vom PATH ab</q> behoben [CVE-2022-4883]">
<correction libzen "Anfälligkeit für Nullzeiger-Dereferenzierung behoben [CVE-2020-36646]">
<correction linux "Neue stabile Version der Originalautoren; ABI auf 22 angehoben; [rt] Aktualisierung auf 5.10.176-rt86">
<correction linux-signed-amd64 "Neue stabile Version der Originalautoren; ABI auf 22 angehoben; [rt] Aktualisierung auf 5.10.176-rt86">
<correction linux-signed-arm64 "Neue stabile Version der Originalautoren; ABI auf 22 angehoben; [rt] Aktualisierung auf 5.10.176-rt86">
<correction linux-signed-i386 "Neue stabile Version der Originalautoren; ABI auf 22 angehoben; [rt] Aktualisierung auf 5.10.176-rt86">
<correction lxc "Datei-Existenz-Orakeln abgeschafft [CVE-2022-47952]">
<correction macromoleculebuilder "Kompilierungsfehlschlag durch Hinzufügen einer Kompilierungs-Abhängigkeit von docbook-xsl behoben">
<correction mariadb-10.5 "Neue stabile Version der Originalautoren; Änderung der Originalautoren an libmariadb-API rückgängig gemacht">
<correction mono "Desktop-Datei entfernt">
<correction ncurses "Schutz vor defekten terminfo-Daten eingebaut [CVE-2022-29458]; tic-Absturz bei sehr langen tc/use-Klauseln behoben">
<correction needrestart "Warnungen bei Verwendung der Option <q>-b</q> behoben">
<correction node-cookiejar "Schutz vor schadhaft großen Cookies eingebaut [CVE-2022-25901]">
<correction node-webpack "Realm-übergreifenden Zugriff auf Objekte unterbunden [CVE-2023-28154]">
<correction nvidia-graphics-drivers "Neue Version der Originalautoren; Sicherheitskorrekturen [CVE-2023-0180 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199]">
<correction nvidia-graphics-drivers-tesla-450 "Neue Version der Originalautoren; Sicherheitskorrekturen [CVE-2023-0180 CVE-2023-0184 CVE-2023-0185 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199]">
<correction nvidia-graphics-drivers-tesla-470 "Neue Version der Originalautoren; Sicherheitskorrekturen [CVE-2023-0180 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199]">
<correction nvidia-modprobe "Neue Version der Originalautoren">
<correction openvswitch "<q>openvswitch-switch-Aktualisierung zieht Schnittstellen nicht hoch</q> behoben">
<correction passenger "Kompatibilität mit neueren NodeJS-Versionen verbessert">
<correction phyx "Unnötige Kompilierungs-Abhängigkeit von libatlas-cpp entfernt">
<correction postfix "Neue stabile Version der Originalautoren">
<correction postgis "Falsche Achsenanordnung in der polaren Stereografie behoben">
<correction postgresql-13 "Neue stabile Version der Originalautoren; Anfälligkeit für Offenlegung des Client-Speichers behoben [CVE-2022-41862]">
<correction python-acme "Version der erzeugten CSRs korrigiert, um Probleme mit Implementierungen der ACME-API, die sich strikt an die RFCs halten, zu beheben">
<correction ruby-aws-sdk-core "Generierung der Versionsdatei überarbeitet">
<correction ruby-cfpropertylist "Ein paar Funktionalitäten durch Aufgeben der Kompatibilität mit Ruby 1.8 wiederhergestellt">
<correction shim "Neue Version der Originalautoren; neue stabile Version der Originalautoren; NX-Unterstützung zur Kompilierungszeit aktiviert; Debian-GRUB-Binärdateien mit sbat &lt; 4 blockiert">
<correction shim-helpers-amd64-signed "Neue stabile Version der Originalautoren; NX-Unterstützung zur Kompilierungszeit aktiviert; Debian-GRUB-Binärdateien mit sbat &lt; 4 blockiert">
<correction shim-helpers-arm64-signed "Neue stabile Version der Originalautoren; NX-Unterstützung zur Kompilierungszeit aktiviert; Debian-GRUB-Binärdateien mit sbat &lt; 4 blockiert">
<correction shim-helpers-i386-signed "Neue stabile Version der Originalautoren; NX-Unterstützung zur Kompilierungszeit aktiviert; Debian-GRUB-Binärdateien mit sbat &lt; 4 blockiert">
<correction shim-signed "Neue stabile Version der Originalautoren; NX-Unterstützung zur Kompilierungszeit aktiviert; Debian-GRUB-Binärdateien mit sbat &lt; 4 blockiert">
<correction snakeyaml "Dienstblockaden behoben [CVE-2022-25857 CVE-2022-38749 CVE-2022-38750 CVE-2022-38751]; Dokumentation beüglich Sicherheitsunterstützung und -Problemen hinzugefügt">
<correction spyder "Code-Dublizierung beim Speichern abgestellt">
<correction symfony "Private Kopfzeilen vor dem Einspeichern von Antworten mit HttpCache entfernen [CVE-2022-24894]; CSRF-Tokens beim erfolgreichen Anmelden vom Speicher entfernen [CVE-2022-24895]">
<correction systemd "Anfälligkeit für Informationslecks [CVE-2022-4415], Dienstblockade behoben [CVE-2022-3821]; ata_id: Abruf des Response Code der SCSI Sense Data überarbeitet; logind: Abruf der Eigenschaft OnExternalPower via D-Bus überarbeitet; Absturz in systemd-machined behoben">
<correction tomcat9 "OpenJDK-17-Unterstützung in JDK-Erkennung hinzugefügt">
<correction traceroute "v4mapped-IPv6-Adressen als IPv4 interpretieren">
<correction tzdata "Enthaltene Daten aktualisiert">
<correction unbound "Angriff via Delegation auf nicht-reagierende DNS-Server behoben [CVE-2022-3204]; Probleme mit <q>Geister-Domain-Namen</q> behoben [CVE-2022-30698 CVE-2022-30699]">
<correction usb.ids "Enthaltene Daten aktualisiert">
<correction vagrant "Unterstützung für VirtualBox 7.0 hinzugefügt">
<correction voms-api-java "Kompilierungsfehlschläge durch Abstellen einiger nicht-funktionierender Tests behoben">
<correction w3m "Schreibzugriff außerhalb der Grenzen behoben [CVE-2022-38223]">
<correction x4d-icons "Kompilierungsfehlschlag mit neueren imagemagick-Versionen behoben">
<correction xapian-core "Datenbank-Beschädigung bei Plattenplatzmangel behoben">
<correction zfs-linux "Mehrere Verbesserungen der Stabilität">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für
jede davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2022 5170 nodejs>
<dsa 2022 5237 firefox-esr>
<dsa 2022 5238 thunderbird>
<dsa 2022 5259 firefox-esr>
<dsa 2022 5262 thunderbird>
<dsa 2022 5282 firefox-esr>
<dsa 2022 5284 thunderbird>
<dsa 2022 5300 pngcheck>
<dsa 2022 5301 firefox-esr>
<dsa 2022 5302 chromium>
<dsa 2022 5303 thunderbird>
<dsa 2022 5304 xorg-server>
<dsa 2022 5305 libksba>
<dsa 2022 5306 gerbv>
<dsa 2022 5307 libcommons-net-java>
<dsa 2022 5308 webkit2gtk>
<dsa 2022 5309 wpewebkit>
<dsa 2022 5310 ruby-image-processing>
<dsa 2023 5311 trafficserver>
<dsa 2023 5312 libjettison-java>
<dsa 2023 5313 hsqldb>
<dsa 2023 5314 emacs>
<dsa 2023 5315 libxstream-java>
<dsa 2023 5316 netty>
<dsa 2023 5317 chromium>
<dsa 2023 5318 lava>
<dsa 2023 5319 openvswitch>
<dsa 2023 5320 tor>
<dsa 2023 5321 sudo>
<dsa 2023 5322 firefox-esr>
<dsa 2023 5323 libitext5-java>
<dsa 2023 5324 linux-signed-amd64>
<dsa 2023 5324 linux-signed-arm64>
<dsa 2023 5324 linux-signed-i386>
<dsa 2023 5324 linux>
<dsa 2023 5325 spip>
<dsa 2023 5326 nodejs>
<dsa 2023 5327 swift>
<dsa 2023 5328 chromium>
<dsa 2023 5329 bind9>
<dsa 2023 5330 curl>
<dsa 2023 5331 openjdk-11>
<dsa 2023 5332 git>
<dsa 2023 5333 tiff>
<dsa 2023 5334 varnish>
<dsa 2023 5335 openjdk-17>
<dsa 2023 5336 glance>
<dsa 2023 5337 nova>
<dsa 2023 5338 cinder>
<dsa 2023 5339 libhtml-stripscripts-perl>
<dsa 2023 5340 webkit2gtk>
<dsa 2023 5341 wpewebkit>
<dsa 2023 5342 xorg-server>
<dsa 2023 5343 openssl>
<dsa 2023 5344 heimdal>
<dsa 2023 5345 chromium>
<dsa 2023 5346 libde265>
<dsa 2023 5347 imagemagick>
<dsa 2023 5348 haproxy>
<dsa 2023 5349 gnutls28>
<dsa 2023 5350 firefox-esr>
<dsa 2023 5351 webkit2gtk>
<dsa 2023 5352 wpewebkit>
<dsa 2023 5353 nss>
<dsa 2023 5355 thunderbird>
<dsa 2023 5356 sox>
<dsa 2023 5357 git>
<dsa 2023 5358 asterisk>
<dsa 2023 5359 chromium>
<dsa 2023 5361 tiff>
<dsa 2023 5362 frr>
<dsa 2023 5363 php7.4>
<dsa 2023 5364 apr-util>
<dsa 2023 5365 curl>
<dsa 2023 5366 multipath-tools>
<dsa 2023 5367 spip>
<dsa 2023 5368 libreswan>
<dsa 2023 5369 syslog-ng>
<dsa 2023 5370 apr>
<dsa 2023 5371 chromium>
<dsa 2023 5372 rails>
<dsa 2023 5373 node-sqlite3>
<dsa 2023 5374 firefox-esr>
<dsa 2023 5375 thunderbird>
<dsa 2023 5376 apache2>
<dsa 2023 5377 chromium>
<dsa 2023 5378 xen>
<dsa 2023 5379 dino-im>
<dsa 2023 5380 xorg-server>
<dsa 2023 5381 tomcat9>
<dsa 2023 5382 cairosvg>
<dsa 2023 5383 ghostscript>
<dsa 2023 5384 openimageio>
<dsa 2023 5385 firefox-esr>
<dsa 2023 5386 chromium>
<dsa 2023 5387 openvswitch>
<dsa 2023 5388 haproxy>
<dsa 2023 5389 rails>
<dsa 2023 5390 chromium>
<dsa 2023 5391 libxml2>
<dsa 2023 5392 thunderbird>
<dsa 2023 5393 chromium>
</table>


<h2>Entfernte Pakete</h2>

<p>Die folgenden Pakete wurden wegen Umständen entfernet, die außerhalb unserer Kontrolle liegen:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction bind-dyndb-ldap "Defekt bei neueren bind9-Versionen; Unterstützung in Stable nicht möglich">
<correction matrix-mirage "Abhängig von python-matrix-io, das entfernt werden soll">
<correction pantalaimon "Abhängig von python-matrix-io, das entfernt werden soll">
<correction python-matrix-nio "Sicherheitsprobleme; funktioniert nicht mit aktuellen Matrix-Servern">
<correction weechat-matrix "Abhängig von python-matrix-io, das entfernt werden soll">
</table>

<h2>Debian-Installer</h2>

<p>Der Installer wurde aktualisiert, damit er die Korrekturen enthält,
die mit dieser Zwischenveröffentlichung in Stable eingeflossen sind.</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert haben:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p></p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern
Freier Software, die ihre Zeit und Mühen einbringen, um das
vollständig freie Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Website unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken Sie eine Mail
(auf Englisch) an &lt;press@debian.org&gt; oder kontaktieren Sie das
Stable-Veröffentlichungs-Team (auf Englisch)
unter &lt;debian-release@lists.debian.org&gt;.</p>



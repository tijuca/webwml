#use wml::debian::translation-check translation="b798c13efccacac13f073ce50e7116c458ba382f" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Buster RC1</define-tag>
<define-tag release_date>2019-04-15</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la première version candidate pour Debian 10 <q>Buster</q>.
</p>


<h2>Modifications importantes dans cette version de l'installateur</h2>

<p>Cette version fournit la prise en charge du démarrage sécurisé (« Secure
Boot ») sur amd64 qui devrait être prêt à l'emploi sur la plupart des machines
où le démarrage sécurisé est activé. Cela signifie qu'il ne devrait plus être
nécessaire de désactiver la prise en charge du démarrage sécurisé dans la
configuration du microprogramme.</p>


<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>apt-setup :
    <ul>
      <li>ajustement de la gestion des ensembles de supports, améliorant la
        prise en charge de Debian Edu.</li>
    </ul>
  </li>
  <li>brltty :
    <ul>
      <li>ajout de la prise en charge de Hedo MobiLine.</li>
    </ul>
  </li>
  <li>cdebconf :
    <ul>
      <li>désactivation de la pagination de l'interface texte pour le moment,
        espeakup ne lit pas les caractères utilisés pour changer de page
        (<a href="https://bugs.debian.org/690343">nº 690343</a>) et donc les
        utilisateurs sont dans la confusion et pensent que la première page est
        l'unique choix.</li>
    </ul>
  </li>
  <li>cryptsetup :
    <ul>
      <li>la version du format de LUKS sur disque est maintenant LUKS2 par
        défaut (<a href="https://bugs.debian.org/919725">nº 919725</a>).</li>
    </ul>
  </li>
  <li>debian-installer :
    <ul>
      <li>passage à l'utilisation du thème graphique de Buster pour l'écran
        initial ;</li>
      <li>passage de la version l'ABI du noyau Linux de 4.19.0-1 à 4.19.0-4 ;</li>
      <li>amélioration des compilations reproductibles (<a href="https://bugs.debian.org/900918">nº 900918</a>,
        <a href="https://bugs.debian.org/920631">nº 920631</a>, <a href="https://bugs.debian.org/920676">nº 920676</a>) ;</li>
      <li>gen-tarball : arrêt de l'utilisation de la variable GZIP dépréciée ;</li>
      <li>gen-tarball : utilisation automatique de pigz s'il est disponible ;</li>
      <li>ajout du sous-menu du thème dark et du raccourci : « d » (<a href="https://bugs.debian.org/820911">nº 820911</a>) ;</li>
      <li>correction du chargement de l'image de fond de grub de l'amorçage
        réseau.</li>
    </ul>
  </li>
  <li>debian-installer-utils :
    <ul>
      <li>arrêt de la propagation de BOOTIF sur le système installé
        (<a href="https://bugs.debian.org/921444">nº 921444</a>).</li>
    </ul>
  </li>
  <li>espeakup :
    <ul>
      <li>correction de l'inscription de l'identifiant de la carte son quand
        l'installation est achevée.</li>
    </ul>
  </li>
  <li>finish-install :
    <ul>
      <li>ajout de la prise en charge de multiples consoles.</li>
    </ul>
  </li>
  <li>grub-installer :
    <ul>
      <li>sur Linux, montage et démontage de /run pour contourner les délais de
        l'initialisation de LVM (<a href="https://bugs.debian.org/918590">nº 918590</a>).</li>
    </ul>
  </li>
  <li>grub2 :
    <ul>
      <li>correction de la configuration sur les systèmes Secure Boot où
        cryptodisk est utilisé (<a href="https://bugs.debian.org/917117">nº 917117</a>) ;</li>
      <li>recommandation par grub-efi-amd64-signed de shim-signed
        (<a href="https://bugs.debian.org/919067">nº 919067</a>) ;</li>
      <li>ajout des modules help et ls aux images UEFI signées (<a href="https://bugs.debian.org/919955">nº 919955</a>) ;</li>
      <li>grub-install : vérification que arm-efi est une cible par défaut
        (<a href="https://bugs.debian.org/922104">nº 922104</a>) ;</li>
      <li>util : détection de davantage d'erreurs d'entrée/sortie, telles qu'un
        espace insuffisant (<a href="https://bugs.debian.org/922741">nº 922741</a>) ;</li>
      <li>utilisation des bibliothèques libefi* pour réduire les écritures sur
        le stockage de variables d'EFI (<a href="https://bugs.debian.org/891434">nº 891434</a>),
        plutôt que de dépendre d'efibootmgr. (Ce dernier devrait être de retour
        dans Recommends dans une version ultérieure, parce que c'est un outil
        pratique de débogage.)</li>
    </ul>
  </li>
  <li>hw-detect :
    <ul>
      <li>utilisation de « modprobe -qn » pour la recherche des modules
        disponibles (<a href="https://bugs.debian.org/870448">nº 870448</a>).</li>
    </ul>
  </li>
  <li>libxinerama :
    <ul>
      <li>correction des dépendances d'udeb (<a href="https://bugs.debian.org/921715">nº 921715</a>).</li>
    </ul>
  </li>
  <li>linux :
    <ul>
      <li>certs : remplacement du certificat de signature de test par un
        certificat de signature de production ;</li>
      <li>certs : inclusion à la fois du certificat racine et du certificat de
        signature directe (<a href="https://bugs.debian.org/924545">nº 924545</a>).</li>
    </ul>
  </li>
  <li>live-installer :
    <ul>
      <li>ajout de calamares-settings-debian comme paquet à supprimer à la fin
        de l'installation.</li>
    </ul>
  </li>
  <li>nano :
    <ul>
      <li>ajout d'un correctif pour éviter de montrer les options <tt>^S</tt>
        et <tt>^Q</tt> dans nano-tiny, parce que leur utilisation peut
        provoquer des problèmes dans certains des environnements de
        l'installateur Debian, comme les installations sur une console série
        (<a href="https://bugs.debian.org/915017">nº 915017</a>).</li>
    </ul>
  </li>
  <li>network-console :
    <ul>
      <li>suppression de la prise en charge du « bip » pour arm*/ixp4xx et
        arm*/iop32x ; il était utilisé pour indiquer aux utilisateurs que leurs
        périphériques étaient prêts à être installés, mais la prise en charge
        de ces plateformes a été retirée du noyau Linux et donc de
        l'installateur Debian (<a href="https://bugs.debian.org/921951">nº 921951</a>).</li>
    </ul>
  </li>
  <li>partman-base :
    <ul>
      <li>assurance que les UUID sont disponibles dans /dev/disk/by-uuid/ de
        sorte que le chargeur d'amorçage peut être configuré pour les utiliser
        (<a href="https://bugs.debian.org/852323">nº 852323</a>).</li>
    </ul>
  </li>
  <li>partman-lvm :
    <ul>
      <li>correction effective des caractères non valables dans les noms de
        groupe de volumes (<a href="https://bugs.debian.org/911036">nº 911036</a>),
        la tentative précédente n'était pas totalement fonctionnelle
        (<a href="https://bugs.debian.org/922230">nº 922230</a>, <a href="https://bugs.debian.org/922100">nº 922100</a>).</li>
    </ul>
  </li>
  <li>partman-partitioning :
    <ul>
      <li>utilisation par défaut de GPT sur armhf/efi, sinon de MSDOS.</li>
    </ul>
  </li>
  <li>rootskel :
    <ul>
      <li>installation de theme=dark et activation de Compiz dans MATE ;</li>
      <li>l'utilisation de theme=dark fait que grub utilise un thème sombre ;</li>
      <li>exécution de l'installateur Debian sur des consoles multiples
        indépendamment de l'endroit où il est configuré ;</li>
      <li>ajout de error-handling à steal-ctty ;</li>
      <li>ajout d'une connexion précoce pour qu'il soit ensuite possible de
        voir quelles consoles sont détectées et choisies.</li>
    </ul>
  </li>
  <li>rootskel-gtk :
    <ul>
      <li>intégration du thème de Buster : futureprototype.</li>
    </ul>
  </li>
  <li>systemd :
    <ul>
      <li>udev : exécution des programmes dans l'ordre indiqué (<a href="https://bugs.debian.org/925190">nº 925190</a>) ;</li>
      <li>udev : suppression en masse empêchée quand il n'est pas exécuté sous
        systemd (<a href="https://bugs.debian.org/918764">nº 918764</a>).</li>
    </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>

<ul>
  <li>base-installer :
    <ul>
      <li>fonctionnement de armhf/efi identique à celui de armhf/generic pour
        la sélection des noyaux.</li>
    </ul>
  </li>
  <li>debian-installer :
    <ul>
      <li>armel : suppression de la liste de paquets inutilisés pour ixp4xx
        (<a href="https://bugs.debian.org/921951">nº 921951</a>) ;</li>
      <li>armhf : ajout d'images pour Novena ;</li>
      <li>armhf : ajout d'images pour BananaPiM2Berry.</li>
    </ul>
  </li>
  <li>flash-kernel :
    <ul>
      <li>ajout d'une entrée dans la base de données de matériel pour Rock64
        (<a href="https://bugs.debian.org/906696">nº 906696</a>) ;</li>
      <li>ajout d'une entrée dans la base de données de matériel pour
        Banana Pi M2 Berry (<a href="https://bugs.debian.org/923483">nº 923483</a>) ;</li>
      <li>ajout d'une entrée dans la base de données de matériel pour la carte
        Pine A64 LTS (<a href="https://bugs.debian.org/923655">nº 923655</a>) ;</li>
      <li>ajout d'une entrée dans la base de données de matériel pour Olimex
        A64 Teres-I (<a href="https://bugs.debian.org/926071">nº 926071</a>) ;</li>
      <li>ajout d'entrées dans la base de données de matériel pour Raspberry
        Pi 1, Pi Zero et Pi 3 Compute Module (<a href="https://bugs.debian.org/921518">nº 921518</a>).</li>
    </ul>
  </li>
  <li>libdebian-installer :
    <ul>
      <li>toutes les machines arm32 avec EFI montrées comme une
        sous-architecture EFI.</li>
    </ul>
  </li>
  <li>linux :
    <ul>
      <li>[armel] ajout de spi-orion à mtd.udeb pour pouvoir accéder à spi
        flash sur par exemple qnap ts-21x (<a href="https://bugs.debian.org/920607">nº 920607</a>) ;</li>
      <li>[armel] udeb : ajout de mmc-core-modules ;</li>
      <li>[arm64,armhf] udeb : ajout de mmc-core-modules à Provides de
        kernel-image ;</li>
      <li>[armhf,riscv64,s390x] udeb : ajout de cdrom-core-modules ;</li>
      <li>[arm64,armhf,ia64,riscv64,sparc64] udeb : ajout de usb-serial-modules
        (<a href="https://bugs.debian.org/903824">nº 903824</a>) ;</li>
      <li>[arm64] udeb : utilisation de ata-modules générique ;</li>
      <li>[arm64] udeb : retrait de lignes redondantes de nic-modules ;</li>
      <li>udeb : ajout de virtio-gpu dans l'installateur Debian pour obtenir
        une sortie graphique dans les instances de VM ;</li>
      <li>udeb : ajout de scsi-nic-modules fournissant les pilotes iSCSI/FC
        de Chelsio et Qlogic ;</li>
      <li>udeb : ajout de fb-modules et inclusion de drm et drm_kms_helper sur
        la plupart des architectures ;</li>
      <li>udeb : déplacement des modules de PV de base de
        {hyperv,virtio}-modules vers kernel-image ;</li>
      <li>udeb : déplacement des pilotes de {hyperv,virtio}-modules vers
        {fb,input,nic,scsi}-modules ;</li>
      <li>udeb : dépendance de nic-wireless-modules de mmc-core-modules, et
        non de mmc-modules ; déplacement de crc7 vers crc-modules pour éviter
        la duplication ;</li>
      <li>udeb : serial_cs rendu optionnel dans serial-modules.</li>
    </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>76 langues sont prises en charge dans cette version ;</li>
  <li>La traduction est complète pour 38 de ces langues.</li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres supports d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>

#use wml::debian::translation-check translation="fce40adff1381ed16a3e5ebae7ad1ff8fcbbb739" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte par Kaspersky Lab dans libvnserver, une
bibliothèque C pour mettre en œuvre les fonctionnalités serveur/client de VNC.
De plus, quelques-unes des vulnérabilités traitées dans DLA 1617-1 avaient des
corrections incomplètes, et ont été traitées dans cette mise à jour.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15126">CVE-2018-15126</a>

<p>Un attaquant peut causer un déni de service ou une exécution de code
à distance à l'aide d'une utilisation de mémoire de tas après libération dans
l’extension tightvnc-filetransfer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20748">CVE-2018-20748</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-20749">CVE-2018-20749</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-20750">CVE-2018-20750</a>

<p>Quelques correctifs d’écriture de tas hors limites pour
<a href="https://security-tracker.debian.org/tracker/CVE-2018-20019">CVE-2018-20019</a> et
<a href="https://security-tracker.debian.org/tracker/CVE-2018-15127">CVE-2018-15127</a>
étaient incomplets. Ces CVE traitent ces problèmes.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 0.9.9+dfsg2-6.1+deb8u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libvncserver.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1652.data"
# $Id: $

#use wml::debian::translation-check translation="525aadda8fdfdc6a27b2cdae7fd3f759628a46ae" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes ont été corrigés dans bash, l’interpréteur de commandes GNU
 Bourne-Again Shell.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9401">CVE-2016-9401</a>

<p>Défaut de segmentation dans popd interne lorsqu’il est appelé avec des décalages
négatifs hors intervalle.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9924">CVE-2019-9924</a>

<p>Sylvain Beucler a découvert qu’il est possible d’appeler des commandes
contenant une barre oblique dans le mode restreint (rbash) en l’ajoutant dans
un tableau BASH_CMDS.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.3-11+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets bash.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1726.data"
# $Id: $

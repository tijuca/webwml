#use wml::debian::translation-check translation="351930f1102705ffe978c0aff9142928e80c98ff" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2156">CVE-2023-2156</a>

<p>Il a été découvert qu’un défaut dans le traitement du protocole RPL pouvait
permettre à un attaquant distant non authentifié de provoquer un déni de service
si RPL était activé (non activé par défaut dans Debian).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3390">CVE-2023-3390</a>

<p>Un défaut d’utilisation de mémoire après libération dans le sous-système
netfilter, causé par un traitement incorrect de chemin d’erreur, pouvait
aboutir à un déni de service ou à une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3610">CVE-2023-3610</a>

<p>Un défaut d'utilisation de mémoire après libération dans le sous-système
netfilter, provoqué par le traitement incorrect de refcount sur le chemin de
<q>destroy</q> de table et <q>chain</q>, pouvait avoir pour conséquences un déni
de service ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-20593">CVE-2023-20593</a>

<p>Tavis Ormandy a découvert que dans certaines circonstances
microarchitecturales, un registre de vecteur dans les CPU AMD <q>Zen 2</q>
n’était par réglé correctement à zéro. Ce défaut permettait à un attaquant de
divulguer des informations sensibles à travers des processus concurrents, des
threads hyper et des invités virtualisés.</p>

<p>Pour plus de détails, consulter
<a href="https://lock.cmpxchg8b.com/zenbleed.html">https://lock.cmpxchg8b.com/zenbleed.html</a> et
<a href="https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8">https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8</a>.</p>

<p>Ce problème peut aussi être atténué par une mise à jour du microcode au moyen
du paquet amd64-microcode ou la mise à jour du microprogramme du système
(BIOS/UEFI). Cependant,la version initiale du microcode d'AMD ne fournit des
mises à jour que pour les processeurs EPYC de deuxième génération: divers
processeurs Ryzen sont aussi affectés, mais les mises à jour ne sont pas encore
disponibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31248">CVE-2023-31248</a>

<p>Mingi Cho a découvert un défaut d’utilisation de mémoire après libération
dans l’implémentation Netfilter de nf_tables lors de l’utilisation de
nft_chain_lookup_byid, qui pouvait aboutir à une élévation locale des privilèges
pour un utilisateur avec la capacité CAP_NET_ADMIN dans tout espace de nommage
utilisateur ou réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35001">CVE-2023-35001</a>

<p>Tanguy DUBROCA a découvert un défaut de lecture et écriture hors limites dans
l’implémentation Netfilter de nf_tables lors du traitement d’une expression
nft_byteorder, qui pouvait aboutir à une élévation locale des privilèges pour
un utilisateur avec la capacité CAP_NET_ADMIN dans tout espace de nommage
utilisateur ou réseau.</p></li>
</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 5.10.179-3~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-5.10.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-5.10,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-5.10">\
https://security-tracker.debian.org/tracker/linux-5.10</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3512.data"
# $Id: $

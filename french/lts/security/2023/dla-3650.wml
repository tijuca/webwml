#use wml::debian::translation-check translation="d031d260c88b817f287768aa66c1131297ceee1e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La bibliothèque audiofile permet le traitement de données audio de et vers
des fichiers audio de formats les plus courants (actuellement AIFF, AIFF-C,
WAVE, NeXT/Sun, BICS et raw).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13147">CVE-2019-13147</a>

<p>Audiofile était vulnérable à cause d’un dépassement d'entier. Il terminait
 trop rapidement si des fichiers NeXT audio incluaient de trop nombreux canaux.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24599">CVE-2022-24599</a>

<p>Une fuite de mémoire a été découverte due à une lecture d’un champ de
copyright non terminé par NUL. Préallocation d’une mémoire remplie de zéros et
terminaison des chaines C toujours par NUL.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.3.6-5+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets audiofile.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de audiofile,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/audiofile">\
https://security-tracker.debian.org/tracker/audiofile</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3650.data"
# $Id: $

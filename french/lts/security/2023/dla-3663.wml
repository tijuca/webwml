#use wml::debian::translation-check translation="d1d64a0d4317c53d9dca4f207d458710dadc4d86" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un dépassement potentiel de tampon dans
strongswan, un serveur VPN (réseau privé virtuel) basé sur IPsec.</p>

<p>Une vulnérabilité relative au traitement de valeurs d’échanges de clé
publique Diffie-Hellman pouvait aboutir à un dépassement de tampon et,
éventuellement, à une exécution de code à distance.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41913">CVE-2023-41913</a>

<p>Une vulnérabilité dans charon-tkm relative au valeurs publiques DH a été
découverte dans strongSwan, qui pouvait aboutir à un dépassement de tampon et,
éventuellement, à une exécution de code à distance. Toutes les versions depuis
5.3.0 sont affectées.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 5.7.2-1+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets strongswan.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3663.data"
# $Id: $

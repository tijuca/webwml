#use wml::debian::translation-check translation="0d84f468c75d625d7303b562589e24d2743ae68f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que lors de l’utilisation des fonctions
get_post_logout_redirect et get_post_login_redirect dans flask-security, une
implémentation de sécurité simple pour les applications Flask, il était possible
de contourner une validation d’URL et rediriger un utilisateur vers un
URL arbitraire en fournissant plusieurs obliques inverses telles que
\\\evil.com/chemin.</p>

<p>Cette vulnérabilité était exploitable seulement si un serveur de remplacement
WSGI autre que Werkzeug était utilisé ou que le comportement par défaut de
Werkzeug était modifié en utilisant <q>autocorrect_location_header=false</q>.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.7.5-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets flask-security.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de flask-security,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/flask-security">\
https://security-tracker.debian.org/tracker/flask-security</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3545.data"
# $Id: $

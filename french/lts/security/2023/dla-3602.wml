#use wml::debian::translation-check translation="707241c5e0bc3a60ce863c2740cf7036ea5c3bd0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans libx11, la bibliothèque X11
côté client.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-43785">CVE-2023-43785</a>

<p>Gregory James Duck a découvert un accès en mémoire hors limites dans
_XkbReadKeySyms, qui pouvait aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-43786">CVE-2023-43786</a>

<p>Yair Mizrahi a trouvé une récursion infinie dans PutSubImage lors de
l’analyse d’un fichier contrefait, qui aboutissait à un épuisement de pile et à
un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-43787">CVE-2023-43787</a>

<p>Yair Mizrahi a découvert un dépassement d'entier dans XCreateImage lors de
l’analyse d’une entrée contrefaite, qui aboutissait à une petite allocation de
tampon amenant à son dépassement. Cela pouvait aboutir à un déni de service ou,
éventuellement, à l’exécution de code arbitraire.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2:1.6.7-1+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libx11.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libx11,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libx11">\
https://security-tracker.debian.org/tracker/libx11</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3602.data"
# $Id: $

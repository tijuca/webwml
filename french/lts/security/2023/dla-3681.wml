#use wml::debian::translation-check translation="03b9b891db1aea7fb6af84ca6caa5f7c4c6715f1" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Amanda, un système de
sauvegarde conçu pour des archives de plusieurs ordinateurs sur un réseau dans
un seul périphérique à bande magnétique de grande capacité. Ces vulnérabilités
permettaient éventuellement une élévation locale des privilèges de l’utilisateur
de sauvegarde à ceux du superutilisateur ou de divulguer si un répertoire
existait dans le système de fichiers.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37703">CVE-2022-37703</a>

<p>Dans Amanda 3.5.1, une vulnérabilité de fuite d'informations a été découverte
dans le binaire calcsize SUID. Un attaquant pouvait profiter de cette
vulnérabilité pour savoir si un répertoire existait ou pas dans le système de
fichiers. Le binaire utiliserait <q>opendir()</q> comme superutilisateur sans
vérifier le chemin, permettant à l’attaquant de fournir un chemin arbitraire.</p>

<p></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37705">CVE-2022-37705</a>

<p>Un défaut d’élévation des privilèges a été découvert dans Amanda 3.5.1 avec
lequel l’utilisateur de sauvegarde pouvait acquérir les droits du
superutilisateur. Le composant vulnérable était le programme runtar SUID, qui
est une enveloppe pour exécuter /usr/bin/tar avec des arguments spécifiques
contrôlables par l’attaquant. Ce programme gèrait incorrectement les arguments
passés au binaire tar.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-30577">CVE-2023-30577</a>

<p>Le binaire SUID <q>runtar</q> pouvait accepter des options de GNU tar
possiblement malveillantes si lui était fourni des options non argument débutant
avec <q>--exclude</q> (par exemple, --exclude-vcs). L’option qui suivait était
acceptée comme <q>valable</q> et cela pouvait être une option passant un
script/binaire qui était exécuté avec les permissions du superutilisateur.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:3.5.1-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets amanda.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de amanda,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/amanda">\
https://security-tracker.debian.org/tracker/amanda</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3681.data"
# $Id: $

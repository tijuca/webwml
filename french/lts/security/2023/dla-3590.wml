#use wml::debian::translation-check translation="17483401e7a4c5cc0f2c630e31024ed3200e1197" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des problèmes de sécurité ont été découverts dans python-reportlab, une
bibliothèque Python pour créer des graphismes et des PDF, qui pouvaient conduire
à une exécution de code à distance ou à un contournement d’autorisation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19450">CVE-2019-19450</a>

<p>Ravi Prakash Giri a découvert une vulnérabilité d’exécution de code à
distance à l’aide d’un document XML contrefait où
<q><code>&lt;unichar code="</code></q> est suivi par du code Python arbitraire.</p>

<p>Ce problème est similaire au
<a href="https://security-tracker.debian.org/tracker/CVE-2019-17626">CVE-2019-17626</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28463">CVE-2020-28463</a>

<p>Karan Bamal a découvert une vulnérabilité de contrefaçon de requête côté
serveur (SSRF) à l’aide de balises <code>&lt;img&gt;</code>. De nouveaux réglages
<code>trustedSchemes</code> et <code>trustedHosts</code> ont été ajoutés dans
le cadre de la correction/mitigation. Ils peuvent être utilisés pour indiquer
une liste explicite des sources distantes autorisées.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 3.5.13-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-reportlab.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-reportlab,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-reportlab">\
https://security-tracker.debian.org/tracker/python-reportlab</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3590.data"
# $Id: $

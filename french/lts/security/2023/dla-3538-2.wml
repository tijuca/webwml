#use wml::debian::translation-check translation="e13eeba6f42a07f1911e2581d3f67d876b6080fc" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La dernière mise à jour nécessitait de mettre à jour le schéma de base de
données, mais comme zabbix ne gère pas la mise à niveau du schéma de base de
données si SQlite3 est utilisé, l’utilisation de zabbix-proxy-sqlite3 requiert
d’abandonner la base de données et de la recréer en utilisant un fichier fourni
de modèle sql.</p>

<p>Comme ce fichier de modèle n’a pas été mis à jour dans la précédente
publication, cette recréation était difficile par méconnaissance des détails.</p>

<p>Veuillez lire /usr/share/doc/zabbix-proxy-sqlite3/README.Debian pour les
instructions de création de ce fichier de base de données.</p>
<p><b>Remarque</b> : toutes les autres bases de données mettent automatiquement
à jour ce schéma.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:4.0.4+dfsg-1+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zabbix.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de zabbix,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/zabbix">\
https://security-tracker.debian.org/tracker/zabbix</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3538-2.data"
# $Id: $

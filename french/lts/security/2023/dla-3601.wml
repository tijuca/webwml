#use wml::debian::translation-check translation="0e74eb0215b9cac4bda9ffcee51a59fda1abb74e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans Thunderbird, qui
pouvaient avoir pour conséquences un déni de service ou l'exécution de code
arbitraire.</p>

<p>Debian suit les versions amont de Thunderbird. Le suivi des séries 102.x est
terminé, aussi à partir de cette mise à jour, Debian suit les versions 115.x.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:115.3.1-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets thunderbird.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de thunderbird,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/thunderbird">\
https://security-tracker.debian.org/tracker/thunderbird</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3601.data"
# $Id: $

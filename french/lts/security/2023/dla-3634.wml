#use wml::debian::translation-check translation="8607eb160a7ecb7073ef88e817d38b74a0682450" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans nss, un ensemble de
bibliothèques conçues pour prendre en charge le développement multiplateforme
d’applications de client et serveur sécurisées.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25648">CVE-2020-25648</a>

<p>Un défaut a été découvert dans la façon dont NSS gérait les messages
CipherChangeSpec dans TLS 1.3. Il pouvait permettre à un attaquant d’envoyer
plusieurs messages CCS aux serveurs compilés avec NSS, provoquant un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4421">CVE-2023-4421</a>

<p>Un projet à données aléatoires a découvert des vulnérabilités à des
attaques temporelles Bleichenbacher dans des utilitaires de NSS pour le
chiffrement.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2:3.42.1-1+deb10u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nss.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nss,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/nss">\
https://security-tracker.debian.org/tracker/nss</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3634.data"
# $Id: $

#use wml::debian::translation-check translation="1004507253d59ced960d295dc66b68f550ff1ac6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une série de vulnérabilités de dépassement
de tas et de dépassement d'entier dans Sofia-SIP, un bloc de construction pour
créer des applications VoIP/SIP et de message instantané.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32307">CVE-2023-32307</a>

<p>Sofia-SIP est une bibliothèque User-Agent SIP au code source ouvert,
conforme à la spécification RFC-3261 de l’IETF. Selon l’annonce
<a href="https://github.com/freeswitch/sofia-sip/security/advisories/GHSA-8599-x7rq-fr54">GHSA-8599-x7rq-fr54</a>
plusieurs autres dépassements potentiels de tas et d’entier dans
<code>stun_parse_attr_error_code</code> et <code>stun_parse_attr_uint32</code>
ont été trouvés à cause d’une absence de vérification de longueur d’attributs
quand Sofia-SIP gérait les paquets STUN. Le correctif précédent
<a href="https://github.com/freeswitch/sofia-sip/security/advisories/GHSA-8599-x7rq-fr54">GHSA-8599-x7rq-fr54</a>
corrigeait la vulnérabilité quand attr_type ne correspondait pas à la valeur
d’énumération, mais il existait aussi des vulnérabilités dans le traitement
d’autres cas valables. La lecture hors limite et un dépassement d'entier
réalisés par l’attaquant pouvaient conduire à un plantage, une consommation
élevée de mémoire ou même à d’autres conséquences plus graves. Ce problème a
été corrigé dans la version 1.13.15. La mise à niveau est conseillée.</li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.12.11+20110422.1-2.1+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sofia-sip.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3441.data"
# $Id: $

#use wml::debian::translation-check translation="08b90e531e236973c174ac37c6305ecb995fd075" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité de déni de service
potentielle (DoS) dans memcached, un système de mise en cache d'objets en mémoire
haute performance.</p>

<p>Un plantage pouvait se produire lors du traitement de téléversements
<q>multi-packet</q> dans le mode UDP. Les déploiements de memcached qui
utilisaient seulement TCP n’étaient pas, très vraisemblablement, touchés par ce
problème.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-48571">CVE-2022-48571</a>

<p>memcached 1.6.7 permettait un déni de service à l’aide de téléversements
multipaquets dans UDP.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.5.6-1.1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets memcached.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3557.data"
# $Id: $

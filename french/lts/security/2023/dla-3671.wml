#use wml::debian::translation-check translation="0bb60b448da8c2c34b995634ff6410ffcbd51c0b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans mediawiki, un moteur de site
web pour travail collaboratif, qui pouvaient conduire à une divulgation
d'informations, à une élévation des privilèges ou à un déni de service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3550">CVE-2023-3550</a>

<p>Carlos Bello a signalé une vulnérabilité de script intersite (XSS) permanent
lors du téléversement d’un fichier XML contrefait vers
<code>Special:Upload</code>, qui pouvait conduire à une élévation des
privilèges (toutefois le téléversement de fichier <code>.xml</code> n’est pas
permis dans la configuration par défaut).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-45362">CVE-2023-45362</a>

<p>Tobias Frei a découvert que diff-multi-sameuser (<q>révisions X
intermédiaires par le même utilisateur non montrées</q>) ignorait la
suppression de nom d’utilisateur, ce qui pouvait conduire à une fuite
d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-45363">CVE-2023-45363</a>

<p>Il a été découvert que la requête de pages redirigées vers d’autres variantes
avec les paramètres <code>redirects</code> et <code>converttitles</code> activés
pouvait provoquer un déni de service (boucle illimitée et
<code>RequestTimeoutException</code>).</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:1.31.16-1+deb10u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mediawiki.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mediawiki,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mediawiki">\
https://security-tracker.debian.org/tracker/mediawiki</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3671.data"
# $Id: $

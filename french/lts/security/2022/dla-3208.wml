#use wml::debian::translation-check translation="eab80064c218e4ffaae98649935aa8eade532f68" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Martin van Kervel Smedshammer a découvert qu’une attaque de contrefaçon de
requête peut être réalisée dans les serveurs de cache Varnish qui ont le
protocole HTTP/2 activé. Un attaquant peut introduire des caractères à travers
les pseudo-en-têtes HTTP/2, qui sont non valables dans le contexte d’une ligne
de requête HTTP/1, faisant que le serveur Varnish produise des requêtes HTTP/1
non valables sur le dorsal. Cela peut être à son tour utilisé pour exploiter
avec succès des vulnérabilités dans un serveur derrière le serveur Varnish.</p>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 6.1.1-1+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets varnish.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de varnish,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/varnish">\
https://security-tracker.debian.org/tracker/varnish</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3208.data"
# $Id: $

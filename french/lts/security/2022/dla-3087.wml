#use wml::debian::translation-check translation="f7b7e2e6ca01177261631b7f0e11b1e622eecad1" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
WebKitGTK :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32893">CVE-2022-32893</a>

<p>Un chercheur anonyme a découvert que le traitement d'un contenu web
contrefait pouvait conduire à l'exécution de code arbitraire. Apple a été
informé d'un rapport indiquant que ce problème a été activement exploité.</p></li>

</ul>

<p>Pour Debian 10 Buster, ce problème a été corrigés dans la version
2.36.7-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3087.data"
# $Id: $

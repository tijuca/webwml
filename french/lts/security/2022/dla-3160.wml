#use wml::debian::translation-check translation="17dd20aa19c6a192b63c6553a502839ac1c980ac" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans la servlet
Tomcat et le moteur JSP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43980">CVE-2021-43980</a>

<p>L'implémentation simplifiée de blocage de lectures et d'écritures
introduite dans Tomcat 10 et rétroportée sur les versions 9.0.47 et
suivantes de Tomcat exposait à un vieux bogue de concurrence (mais
extrêmement difficile à déclencher) qui pouvait faire que les connexions
d'un client partagent une instance de Http11Processor avec pour conséquence
la réception de réponses ou de fragments de réponses par le mauvais client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23181">CVE-2022-23181</a>

<p>Le correctif pour le bogue
<a href="https://security-tracker.debian.org/tracker/CVE-2020-9484">CVE-2020-9484</a>
introduisait une vulnérabilité de compétition entre la vérification et
l'utilisation dans Apache Tomcat qui permettait à un attaquant local de
réaliser des actions avec les privilèges de l'utilisateur que le processus
de Tomcat utilise. Ce problème est seulement exploitable quand Tomcat est
configuré pour des sessions persistantes utilisant le FileStore.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29885">CVE-2022-29885</a>

<p>La documentation d'Apache Tomcat pour l'EncryptInterceptor déclarait
à tort qu'il permettait aux grappes de Tomcat de fonctionner sur un réseau
non sûr. Ce n'était pas exact. Même si l'EncryptInterceptor fournit bien
une protection de confidentialité et d'intégrité, il ne protège pas contre
tous les risques associés à une exécution sur un réseau non sûr, en
particulier contre les risques de déni de service.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
9.0.31-1~deb10u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tomcat9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/tomcat9">\
https://security-tracker.debian.org/tracker/tomcat9</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3160.data"
# $Id: $

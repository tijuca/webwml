#use wml::debian::translation-check translation="d2e2dde58ae5a448da927999a379aab71465b6a7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>
Il a été découvert qu’il existait un problème potentiel d’expansion d’entité
dans libjdom1-java, une bibliothèque rapide et légère pour l’utilisation
d’XML. Des attaquants pourraient causer un déni de service à l'aide d'une
requête HTTP contrefaite pour l'occasion.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33813">CVE-2021-33813</a>

<p>Un problème d’expansion d’entité dans SAXBuilder dans JDOM jusqu’à la
version 2.0.6 permet à des attaquants de provoquer un déni de service à l'aide
d'un requête HTTP contrefaite.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.1.3-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libjdom1-java.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2712.data"
# $Id: $

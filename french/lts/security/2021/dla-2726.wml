#use wml::debian::translation-check translation="0f37d9faa08ef2da6cd2a68dc59721c19da1ddbf" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert qu’il existait deux problèmes dans shiro, un cadriciel
de sécurité pour les applications Java.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13933">CVE-2020-13933</a>

<p>Correction du contournement d’authentification réalisable à l’aide d’une
requête HTTP contrefaite pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17510">CVE-2020-17510</a>

<p>Correction du contournement d’authentification réalisable à l’aide d’une
requête HTTP contrefaite pour l'occasion.</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 1.3.2-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets shiro.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de shiro,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/shiro">\
https://security-tracker.debian.org/tracker/shiro</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2726.data"
# $Id: $

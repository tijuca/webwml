#use wml::debian::translation-check translation="e9cf468fd8cf5824bfdcf672172fbe4af548a210" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Zabbix, une solution de
supervision réseau. Un attaquant peut lister les utilisateurs autorisés ou
rediriger les liens externes à travers le frontal web de zabbix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15132">CVE-2019-15132</a>

<p>Zabbix permet de lister les utilisateurs. Avec des requêtes de connexion,
il est possible d’énumérer les noms d’utilisateur d’application en se basant
sur la variabilité des réponses de serveur (par exemple, les messages
<q>Nom de connexion ou mot de passe incorrect</q> et <q>Accès au système non
autorisé</q> ou simplement le blocage pour quelques secondes). Cela affecte
à la fois api_jsonrpc.php et index.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15803">CVE-2020-15803</a>

<p>Zabbix permet de stocker un script intersite dans un composant d’URL. Ce
correctif avait été abandonné par erreur dans la publication précédente 1:3.0.31+dfsg-0+deb9u1.</p></li>

</ul>

<p>Cette mise à jour fournit aussi plusieurs corrections de bogues et plusieurs
améliorations. Pour plus d’informations, veuillez vous référer au journal de
modifications de l’amont.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:3.0.32+dfsg-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zabbix.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de zabbix, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/zabbix">\
https://security-tracker.debian.org/tracker/zabbix</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2631.data"
# $Id: $

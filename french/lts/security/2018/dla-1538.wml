#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans tinc, un démon de réseau
privé virtuel (VPN). Le projet « Common vulnérabilités et Exposures » (CVE)
identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16737">CVE-2018-16737</a>

<p>Michael Yonli a découvert un défaut dans l’implémentation du protocole
d’authentification qui pourrait permettre à un attaquant distant d’établir une
connexion à sens unique authentifiée avec un autre nœud.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16758">CVE-2018-16758</a>

<p>Michael Yonli a découvert qu’un homme du milieu interceptant une connexion
TCP pouvait être capable de désactiver le chiffrement de paquets UDP envoyés par
un nœud.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.0.24-2+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets tinc.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1538.data"
# $Id: $

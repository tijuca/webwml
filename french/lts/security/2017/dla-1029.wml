#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Libmtp, une bibliothèque pour communiquer avec les appareils gérant MTP (tels
les téléphones cellulaires et les lecteurs de musique), a été découverte comme
étant sensible à plusieurs vulnérabilités de dépassement d'entier qui permettent
à des appareils malveillants de provoquer un déni de service par plantage et
peut être une exécution de code à distance.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9831">CVE-2017-9831</a>

<p>Une vulnérabilité de dépassement d’entier dans la fonction
ptp_unpack_EOS_CustomFuncEx du fichier ptp-pack.c de libmtp (version 1.1.12 et
antérieures) permet à des attaquants de provoquer un déni de service (accès en
mémoire hors limites) ou une exécution possible de code à distance par insertion
d’un appareil mobile dans un ordinateur personnel à l’aide d’un câble USB.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9832">CVE-2017-9832</a>

<p>Une vulnérabilité de dépassement d’entier dans ptp-pack.c (fonction
ptp_unpack_OPL) de libmtp (version 1.1.12 et antérieures) permet à des
attaquants de provoquer un déni de service (accès en mémoire hors limites) ou
une exécution possible de code à distance par insertion d’un appareil mobile
dans un ordinateur personnel à l’aide d’un câble USB.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.1.3-35-g0ece104-5+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libmtp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1029.data"
# $Id: $

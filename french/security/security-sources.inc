#use wml::debian::translation-check translation="3121c231cfff9a41f6abfc6afdfeb2aa9435046e" maintainer="Jean-Pierre Giraud"

<ul>
<li><a href="https://security-tracker.debian.org/">Système de suivi en sécurité de Debian</a>,
    source principale de toutes les informations relatives à la sécurité, avec
    options de recherche
</li>

<li><a href="https://security-tracker.debian.org/tracker/data/json">Liste JSON</a>
    contenant les descriptions de CVE, les noms de paquet, les numéros de
    bogue, les versions de paquet avec correctif, sans les DSA
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DSA/list">Liste des DSA</a>
    contenant les DSA avec leur date, les numéros de CVE relatifs, les versions
    de paquet avec correctif
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DLA/list">Liste des DLA</a>
    contenant les DLA avec leur date, les numéros de CVE correspondants, les versions de
    paquet avec correctif
</li>

<li><a href="https://lists.debian.org/debian-security-announce/">
    Annonces de DSA</a>
</li>
    
<li><a href="https://lists.debian.org/debian-lts-announce/">
    Annonces de DLA</a>
</li>

<li><a href="oval">Fichiers Oval</a></li>

<li>Recherche de DSA (les capitales sont importantes)<br>
    par exemple <tt>https://security-tracker.debian.org/tracker/DSA-3814</tt>
</li>

<li>Recherche de DLA ( -1 est important)<br>
    par exemple <tt>https://security-tracker.debian.org/tracker/DLA-867-1</tt>
</li>

<li>Recherche de CVE<br>
    par exemple <tt>https://security-tracker.debian.org/tracker/CVE-2017-6827</tt>
</li>
</ul>

#use wml::debian::translation-check translation="bb55d6d0b2ff742e0942c4f18c2f2571cc0b5cda" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
WebKitGTK :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41983">CVE-2023-41983</a>

<p>Junsung Lee a découvert que le traitement d'un contenu web pouvait
conduire à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-42852">CVE-2023-42852</a>

<p>Un chercheur anonyme a découvert que le traitement d'un contenu web
pouvait conduire à l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 2.42.2-1~deb11u1.</p>

<p>Pour la distribution stable (Bookworm), ces problèmes ont été corrigés
dans la version 2.42.2-1~deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5557.data"
# $Id: $

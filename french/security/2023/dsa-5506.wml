#use wml::debian::translation-check translation="97b85fad8d5297cd218137e6a5e7bfbee77c2256" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans le navigateur
web Firefox de Mozilla qui pouvaient éventuellement avoir pour conséquence
 l'exécution de code arbitraire.</p>

<p>Debian suit les éditions longue durée (ESR, « Extended Support
Release ») de Firefox. Le suivi des séries 102.x est terminé, aussi, à
partir de cette mise à jour, Debian suit les versions 115.</p>

<p>Entre les versions 102.x et 115.x, Firefox a vu nombre de ses fonctions
mises à jour. Pour davantage d'informations, veuillez consulter la page
<a href="https://www.mozilla.org/en-US/firefox/115.0esr/releasenotes/">\
https://www.mozilla.org/en-US/firefox/115.0esr/releasenotes/</a>.</p>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 115.3.0esr-1~deb11u1.</p>

<p>Pour la distribution stable (Bookworm), ces problèmes ont été corrigés
dans la version 115.3.0esr-1~deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firefox-esr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firefox-esr,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5506.data"
# $Id: $

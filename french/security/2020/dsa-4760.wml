#use wml::debian::translation-check translation="669c87408de3af72c047aaa7ef3786903984b7ba" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans QEMU, un
émulateur de processeur rapide :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12829">CVE-2020-12829</a>

<p>Un dépassement d'entier dans le périphérique d'affichage sm501 peut
provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14364">CVE-2020-14364</a>

<p>Une écriture hors limites dans le code d'émulation d'USB peut provoquer
l'exécution de code du client vers l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15863">CVE-2020-15863</a>

<p>Un dépassement de tampon dans le périphérique réseau XGMAC peut
provoquer un déni de service ou l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16092">CVE-2020-16092</a>

<p>Une assertion déclenchable par le client dans les périphériques e1000e
et vmxnet3 peut provoquer un déni de service.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 1:3.1+dfsg-8+deb10u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de qemu, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/qemu">\
https://security-tracker.debian.org/tracker/qemu</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4760.data"
# $Id: $

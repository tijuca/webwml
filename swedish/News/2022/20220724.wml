#use wml::debian::translation-check translation="925ae76facb2d1bc28343b8f7956afe7f13c0a12"
<define-tag pagetitle>DebConf22 avslutas i Prizren och datumen för DebConf23 tillkännages</define-tag>

<define-tag release_date>2022-07-24</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<p>
Idag, söndagen 24 juli 2022, avslutades den årliga Debian utvecklar-
och bidragslämnarkonferensen. 
Värd för 260 deltagare från 38 olika länder över 91 kombinerade
evenemangsamtal, diskussionssessioner, Birds of a Feather-möten (BoF) och
andra aktiviteter var <a href="https://debconf22.debconf.org">DebConf22</a>
mycket framgångsrikt.
</p>

<p>
Konferensen föregicks av det årliga DebCamp mellan 10 juli och 16 juli som
fokuserade på individuellt arbete och gruppsprintar för personligt samarbete
mot utveckling av Debian.

För att specificera har det detta år hållits sprintar för att avancera
utvecklingen av Mobian/Debian på mobiler, reproducerbara byggen och
Python i Debian, och en BootCamp för nybörjare, för att introduceras för
Debian och få lite praktisk erfarenhet i användning och bidragslämnande
till gemenskapen.
</p>

<p>
Den faktiska Debianutvecklarkonferensen började söndagen 17 juli 2022.

Tillsammans med aktiviteter så som det traditionella 'Bitar från 
DPL'-föreläsningen, den kontinuerliga nyckelsigneringsfesten, lightning talks
och tillkännagivandet av nästa års DebConf (<a 
href="https://wiki.debian.org/DebConf/23">DebConf23</a> i Kochi, Indien,
var det flera sesioner relaterade till programmeringsspråkgrupper så som
Python, Perl och Ruby, så väl som nyhetsuppdateringar för flera projekt
och interna Debianteam, diskussionssessioner (BoFs) från många tekniska
grupper (Långtidsstödsgruppen, Androidverktyg, Debianderivat,
Gruppen för Debianinstalleraren och avbildningsfiler, Debian Science...) och
lokala gemenskaper (Debian Brasil, Debian India, de lokala debiangrupperna),
tillsammans med många andra evenemang av intresse för Debian och fri 
mjukvara.</p>

<p>
<a href="https://debconf22.debconf.org/schedule/">Schemat</a>
uppdaterades varje dag med planerade och ad-hoc-aktiviteter som introducerades
av deltagare under tiden för hela konferensen. Flera aktiviteter som inte kunde
organiseras under föregående år på grund av COVID-pandemin återkom till
konferensens schema: en jobbmässa, öppen mick och poesiafton, den
traditionella ost och vinfesten, gruppfoton och dagsturen.
</p>

<p>
För de som inte hade möjlighet att delta liveströmmades föreläsningarna och
sessionerna och spelades in och görs tillgängliga på
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2022/DebConf22/">
sidan för Debian mötesarkiv</a>.
Nästan alla sessioner tillät fjärrdeltagande via IRC-meddelanden eller
onlinesamarbetsdokument.
</p>

<p>
<a href="https://debconf22.debconf.org/">Webbplatsen för DebConf22</a>
kommer att hållas aktiv för arkiveringsändamål och kommer att forsätta
erbjuda länkar till presentationer och videos av föreläsningar och
evenemang.
</p>

<p>
Nästa år kommer <a href="https://wiki.debian.org/DebConf/23">DebConf23</a> att
hållas i Kochi, Indien, från 10 september till 16 september, 2023.
Enligt traditionen kommer de lokala arrangörerna före DebConf att starta
konferensaktiviteterna med DebCamp (3 september till 9 september, 2023),
med speciellt fokus på individuellt och grupparbete för att förbättra
distributionen.
</p>

<p>
DebConf arbetar för en säker och välkomnande miljö för alla deltagare.
Se <a href="https://debconf22.debconf.org/about/coc/">webbsidan om
förhållningsregler på webbplatsen för DebConf22</a> för ytterligare detaljer
om detta.
</p>

<p>
Debian tackar för åtagandet från flera <a href="https://debconf22.debconf.org/sponsors/">sponsorer</a>
för deras stöd för DebConf22, speciellt våra Platinasponsorer:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://itp-prizren.com/">ITP Prizren</a>
och <a href="https://google.com/">Google</a>

</p>

<h2>Om Debian</h2>

<p>
	Debianprojektet grundades 1993 av Ian Murdock med målsättningen att vara ett
	i sanning fritt gemenskapsprojekt. Sedan dess har projektet vuxit till att
	vara ett av världens största och mest inflytelserika öppenkällkodsprojekt.
	Tusentals frivilliga från hela världen jobbar tillsammans för att skapa
	och underhålla Debianmjukvara. Tillgängligt i 70 språk, och med stöd för
	en stor mängd datortyper, kallar sig Debian det <q>universella
	operativsystemet</q>.
</p>

<h2>Om DebConf</h2>

<p>DebConf är Debianprojektets utvecklarkonferens. Utöver ett fullspäckat
schema med tekniska, sociala och policytal, tillhandahåller DebConf en
möjlighet för utvecklare, bidragslämnare och andra intresserade att
mötas personligen och jobba tillsammans. Det är ägt rum årligen sedan
2000 på så vitt skiljda platser som Scotland, Argentina, och
Bosnien och Herzegovina. Mer information om DebConf finns tillgänglig
på <a href="http://debconf.org/">http://debconf.org</a>.</p>


<h2>Om Lenovo</h2>

<p>
Som en global teknologiledare som tillverkar en bredd av anslutna produkter,
inklusive mobiltelefoner, plattor, PCs och arbetsstationer så väl som
AR/VR-enheter, smarta hem-/kontors-, och datacenterlösningar, förstår
<a href="https://www.lenovo.com">Lenovo</a> hur kritiskt det är med öppna
system och plattformar för en ansluten värld.
</p>


<h2>Om Infomaniak</h2>
<p>
<a href="https://www.infomaniak.com">Infomaniak</a> är Schweiz största
webbhotell, som även erbjuder backup- och
lagringstjänster, lösningar för organisatörer av evenemang, live-strömning och
video on demand-tjänster. Företaget äger sin datahallar och alla element som är
kritiska för driften av tjänsterna och produkterna som tillhandahålls (både
mjukvara och hårdvara).
</p>


<h2>Om ITP Prizren</h2>
<p>
<a href="https://itp-prizren.com/">Innovation and Training Park Prizren</a> har
för avsikt att vara ett föränderligt
och stärkande element inom Information och Kommunikationsteknologi, agro-food
och kreativa industrier, genom skapande och förvaltning av en gynnsam miljö och
effektiva tjänster för små och medelstora företag, utnyttja olika typer av
innovationer som kan bidra till Kosovo för att förbättra utvecklingsnivån inom
industri och forskning, vilket ger fördelar till ekonomin och landet i stort.
</p>

<h2>Om Google</h2>
<p>
<a href="https://google.com/">Google</a> är ett av världens störta teknologiföretag,
som tillhandahåller en bredd av Internetrelaterade tjänster och produkter så
som onlineannonseringsteknik, sökning, cloud computing, mjukvara och hårdvara.
</p>

<p>
Google har stöttat Debian genom att sponsra DebConf i mer än tio år och är även
Debianpartner som sponsrar delar av
<a href="https://salsa.debian.org">Salsa</a>'s infrastruktur för
kontinuerliga integrering inom Googles molnplattform.
</p>


<h2>Kontaktinformation</h2>

<p>För ytterligare information, var vänlig besök DebConf22s webbsida på
<a href="https://debconf22.debconf.org/">https://debconf22.debconf.org/</a>
eller skicka e-post (på engelska) till &lt;press@debian.org&gt;.</p>

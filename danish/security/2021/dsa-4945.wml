#use wml::debian::translation-check translation="79d3502f9416ddb5b313214ab9f721d43b0d4107" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Følgende sårbarheder er opdaget i webmotoren webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21775">CVE-2021-21775</a>

    <p>Marcin Towalski opdagede at en særligt fremstillet webside potentielt 
    kunne føre til informationslækage og yderligere hukommelseskorruption.  For 
    at udløse sårbarheden, skulle et offer narres til at besøge en ondsindet 
    webside.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21779">CVE-2021-21779</a>

    <p>Marcin Towalski opdagede at en særligt fremstillet webside potentielt 
    kunne føre til informationslækage og yderligere hukommelseskorruption.  For 
    at udløse sårbarheden, skulle et offer narres til at besøge en ondsindet 
    webside.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30663">CVE-2021-30663</a>

    <p>En anonym efterforsker opdagede at behandling af ondsindet fabrikeret 
    webindhold kunne føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30665">CVE-2021-30665</a>

    <p>yangkang opdagede at behandling af ondsindet fabrikeret webindhold kunne 
    føre til udførelse af vilkårlig kode.  Apple er opmærksom på en rapport om 
    at dette problem i praksis har været udnyttet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30689">CVE-2021-30689</a>

    <p>En anonym efterforsker opdagede at ondsindet fabrikeret webindhold kunne 
    føre til universiel udførelse af skripter på tværs af servere.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30720">CVE-2021-30720</a>

    <p>David Schutz opdagede at et ondsindet websted kunne være i stand til at 
    tilgå porte med begrænset adgang på vilkårlige servere.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30734">CVE-2021-30734</a>

    <p>Jack Dates opdagede at behandling af ondsindet fabrikeret indhold kunne 
    føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30744">CVE-2021-30744</a>

    <p>Dan Hite opdagede at behandling af ondsindet fabrikeret webindhold kunne 
    føre til universel udførelse af skripter på tværs af servere.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30749">CVE-2021-30749</a>

    <p>En anonym efterforsker opdagede at behandling af ondsindet fabrikeret 
    webindhold kunne føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30758">CVE-2021-30758</a>

    <p>Christoph Guttandin opdagede at behandling af ondsindet fabrikeret 
    webindhold kunne føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30795">CVE-2021-30795</a>

    <p>Sergei Glazunov opdagede at behandling af ondsindet fabrikeret webindhold 
    kunne føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30797">CVE-2021-30797</a>

    <p>Ivan Fratric opdagede at behandling af ondsindet fabrikeret webindhold 
    kunne føre til udførelse af kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30799">CVE-2021-30799</a>

    <p>Sergei Glazunov opdagede at behandling af ondsindet fabrikeret 
    webindhold kunne føre til udførelse af vilkårlig kode.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 2.32.3-1~deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine webkit2gtk-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende webkit2gtk, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4945.data"

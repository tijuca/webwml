#use wml::debian::template title="Beveiligingsinformatie" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="c9366063243a1d6e2294fc8ccd4eae1bf6812255"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Uw Debian-systeem veilig houden</a></li>
<li><a href="#DSAS">Recente adviezen</a></li>
<li><a href="#infos">Bronnen van beveiligingsinformatie</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian neemt beveiliging zeer serieus. We behandelen alle beveiligingsproblemen die onder onze aandacht worden gebracht en zorgen ervoor dat ze binnen een redelijk tijdsbestek worden verholpen.</p>
</aside>

<p>
De ervaring heeft geleerd dat <q>beveiliging door duisterheid</q> nooit werkt. Daarom maakt openbaarmaking snellere en betere oplossingen voor veiligheidsproblemen mogelijk. In dat opzicht gaat deze pagina in op de status van Debian met betrekking tot verschillende bekende beveiligingslekken, die mogelijk gevolgen kunnen hebben voor het Debian-besturingssysteem.
</p>

<p>
Het Debian-project coördineert veel beveiligingsadviezen met andere leveranciers van vrije software, en als gevolg daarvan worden deze adviezen gepubliceerd op dezelfde dag dat een kwetsbaarheid openbaar wordt gemaakt.
Om de nieuwste veiligheidsadviezen van Debian te ontvangen, kunt u zich aanmelden voor de mailinglijst <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
</p>


# "reasonable timeframe" might be too vague, but we don't have
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.


<p>
Debian neemt ook deel aan inspanningen om beveiliging te standaardiseren:
</p>

<ul>
  <li>De <a href="#DSAS">Beveiligingsadviezen van Debian</a> zijn <a href="cve-compatibility">CVE-compatibel</a> (bekijk de <a href="crossreferences">kruisverwijzingen</a>).</li>
  <li>Debian <a href="oval/">publiceert</a> zijn beveiligingsinformatie met behulp van de <a href="https://github.com/CISecurity/OVALRepo">Open Vulnerability Assessment Language (OVAL)</a> (De Open Taal voor Kwetsbaarheidsbeoordeling).</li>
</ul>

<h2><a id="keeping-secure">Uw Debian-systeem veilig houden</a></h2>



<p>

De pakketten <a href="https://packages.debian.org/stable/admin/unattended-upgrades">unattended-upgrades</a> kunnen worden geïnstalleerd om de computer automatisch up-to-date te houden met de nieuwste beveiligingsupdates (en andere updates).

Het <a href="https://wiki.debian.org/UnattendedUpgrades">item op de wiki</a> heeft
meer gedetailleerde informatie over het handmatig instellen van <tt>unattended-upgrades</tt>.

<p>
Voor meer informatie over beveiligingsproblemen in Debian kunt u onze FAQ en onze documentatie raadplegen:
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">Debian beveiligingsFAQ</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">Securing Debian (in het Engels)</a></button></p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Recente adviezen</a> <a class="rss_logo" style="float: none;" href="dsa">RSS</a></h2>

<p>Dit zijn de recente beveiligingsadviezen (DSA) van Debian die op de lijst <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a> werden geplaatst.
<br><b>T</b> is de link naar de informatie van het <a href="https://security-tracker.debian.org/tracker">Beveiligingsvolgsysteem van Debian</a>, het DSA-nummer linkt naar de aankondigingsmail.

<p>
#include "$(ENGLISHDIR)/security/dsa.list"
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="dsa-long">
:#rss#}

<h2><a id="infos">Bronnen van beveiligingsinformatie</a></h2>
#include "security-sources.inc"

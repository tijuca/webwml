<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Clement Lecigne discovered a heap-based buffer overflow in libvpx, a
multimedia library for the VP8 and VP9 video codecs, which may result in
the execution of arbitrary code if a specially crafted VP8 media stream
is processed.</p>

<p>For the oldstable distribution (bullseye), this problem has been fixed
in version 1.9.0-1+deb11u1.</p>

<p>For the stable distribution (bookworm), this problem has been fixed in
version 1.12.0-1+deb12u1.</p>

<p>We recommend that you upgrade your libvpx packages.</p>

<p>For the detailed security status of libvpx please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/libvpx">\
https://security-tracker.debian.org/tracker/libvpx</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5510.data"
# $Id: $

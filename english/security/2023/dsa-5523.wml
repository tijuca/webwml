<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two security issues were found in Curl, an easy-to-use client-side URL
transfer library and command line tool:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38545">CVE-2023-38545</a>

    <p>Jay Satiro discovered a buffer overflow in the SOCKS5 proxy handshake.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38546">CVE-2023-38546</a>

    <p>It was discovered that under some circumstances libcurl was
    susceptible to cookie injection.</p></li>

</ul>

<p>For the oldstable distribution (bullseye), these problems have been fixed
in version 7.74.0-1.3+deb11u10.</p>

<p>For the stable distribution (bookworm), these problems have been fixed in
version 7.88.1-10+deb12u4.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>For the detailed security status of curl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5523.data"
# $Id: $

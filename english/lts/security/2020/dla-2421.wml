<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in cimg, a powerful image processing
library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010174">CVE-2019-1010174</a>

<p>is related to a missing string sanitization on URLs,
which might result in a command injection when loading a special crafted
image.</p>

<p>The other CVEs are about heap-based buffer over-reads or double frees when
loading a crafted image.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
1.7.9+dfsg-1+deb9u1.</p>

<p>We recommend that you upgrade your cimg packages.</p>

<p>For the detailed security status of cimg please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cimg">https://security-tracker.debian.org/tracker/cimg</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2421.data"
# $Id: $

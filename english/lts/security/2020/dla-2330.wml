<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Brief introduction</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17742">CVE-2017-17742</a>

    <p>Response Splitting attack in the HTTP server of WEBrick.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8320">CVE-2019-8320</a>

    <p>Delete directory using symlink when decompressing tar.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8321">CVE-2019-8321</a>

    <p>Escape sequence injection vulnerability in verbose.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8322">CVE-2019-8322</a>

    <p>Escape sequence injection vulnerability in gem owner.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8323">CVE-2019-8323</a>

    <p>Escape sequence injection vulnerability in API response handling.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8324">CVE-2019-8324</a>

    <p>Installing a malicious gem may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8325">CVE-2019-8325</a>

    <p>Escape sequence injection vulnerability in errors.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16201">CVE-2019-16201</a>

    <p>Regular Expression Denial of Service vulnerability of WEBrick's Digest access authentication.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16254">CVE-2019-16254</a>

    <p>HTTP Response Splitting attack in the HTTP server of WEBrick.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16255">CVE-2019-16255</a>

    <p>Code injection vulnerability.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.7.26-1+deb9u2.</p>

<p>We recommend that you upgrade your jruby packages.</p>

<p>For the detailed security status of jruby please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/jruby">https://security-tracker.debian.org/tracker/jruby</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2330.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Stack overflow due to infinite recursion was fixed in agg,
the Anti-Grain Geometry graphical toolkit.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.5+dfsg1-11+deb9u1.</p>

<p>We recommend that you upgrade your agg packages.</p>

<p>For the detailed security status of agg please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/agg">https://security-tracker.debian.org/tracker/agg</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2872.data"
# $Id: $

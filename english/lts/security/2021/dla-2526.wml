<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In Redcarpet before version 3.5.1, there is an injection
vulnerability which can enable a cross-site scripting attack.</p>

<p>In affected versions, no HTML escaping was being performed when
processing quotes. This applies even when the `:escape_html`
option was being used.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.3.4-2+deb9u1.</p>

<p>We recommend that you upgrade your ruby-redcarpet packages.</p>

<p>For the detailed security status of ruby-redcarpet please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-redcarpet">https://security-tracker.debian.org/tracker/ruby-redcarpet</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2526.data"
# $Id: $

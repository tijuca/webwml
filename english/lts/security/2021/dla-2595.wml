<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential arbitrary code execution
vulnerability in velocity, a Java-based template engine for writing web
applications. It could be exploited by applications which allowed untrusted
users to upload/modify templates.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13936">CVE-2020-13936</a>

    <p>An attacker that is able to modify Velocity templates may execute
    arbitrary Java code or run arbitrary system commands with the same
    privileges as the account running the Servlet container. This applies to
    applications that allow untrusted users to upload/modify velocity templates
    running Apache Velocity Engine versions up to 2.2.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1.7-5+deb9u1.</p>

<p>We recommend that you upgrade your velocity packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2595.data"
# $Id: $

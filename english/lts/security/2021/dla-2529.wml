<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>rfc822.c in Mutt through 2.0.4 allows remote attackers to
cause a denial of service (mailbox unavailability) by sending
email messages with sequences of semicolon characters in
RFC822 address fields (aka terminators of empty groups).</p>

<p>A small email message from the attacker can cause large
memory consumption, and the victim may then be unable to
see email messages from other persons.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.7.2-1+deb9u5.</p>

<p>We recommend that you upgrade your mutt packages.</p>

<p>For the detailed security status of mutt please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mutt">https://security-tracker.debian.org/tracker/mutt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2529.data"
# $Id: $

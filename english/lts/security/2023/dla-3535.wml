<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that UnRAR, an unarchiver for rar files, allows extraction of
files outside of the destination folder via symlink chains.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1:5.6.6-1+deb10u3.</p>

<p>We recommend that you upgrade your unrar-nonfree packages.</p>

<p>For the detailed security status of unrar-nonfree please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/unrar-nonfree">https://security-tracker.debian.org/tracker/unrar-nonfree</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3535.data"
# $Id: $

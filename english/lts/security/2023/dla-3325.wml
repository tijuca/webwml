<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in OpenSSL, a Secure
Sockets Layer toolkit, which may result in incomplete encryption, side
channel attacks, denial of service or information disclosure.</p>

<p>Additional details can be found in the upstream advisories at
<a href="https://www.openssl.org/news/secadv/20220705.txt">https://www.openssl.org/news/secadv/20220705.txt</a> and
<a href="https://www.openssl.org/news/secadv/20230207.txt">https://www.openssl.org/news/secadv/20230207.txt</a></p>

<p>For Debian 10 buster, these problems have been fixed in version
1.1.1n-0+deb10u4.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>For the detailed security status of openssl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl">https://security-tracker.debian.org/tracker/openssl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3325.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several out-of-memory, stack overflow or HTTP request smuggling vulnerabilities
have been discovered in Netty, a Java NIO client/server socket framework, which
may allow attackers to cause a denial of service or bypass restrictions when
used as a proxy.</p>

<p>For Debian 10 buster, these problems have been fixed in version
1:4.1.33-1+deb10u3.</p>

<p>We recommend that you upgrade your netty packages.</p>

<p>For the detailed security status of netty please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/netty">https://security-tracker.debian.org/tracker/netty</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3268.data"
# $Id: $

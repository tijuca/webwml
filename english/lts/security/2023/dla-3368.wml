<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in LibreOffice an
office productivity software suite, leading to arbitrary script
execution, improper certificate validation, and weak encryption
of password storage in the user’s configuration database.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25636">CVE-2021-25636</a>

    <p>Only use X509Data
    LibreOffice supports digital signatures of ODF documents and macros
    within documents, presenting visual aids that no alteration of the
    document occurred since the last signing and that the signature is
    valid. An Improper Certificate Validation vulnerability in LibreOffice
    allowed an attacker to create a digitally signed ODF document, by
    manipulating the documentsignatures.xml or macrosignatures.xml stream
    within the document to contain both <q>X509Data</q> and <q>KeyValue</q> children
    of the <q>KeyInfo</q> tag, which when opened caused LibreOffice to verify
    using the <q>KeyValue</q> but to report verification with the unrelated
    <q>X509Data</q> value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3140">CVE-2022-3140</a>

    <p>Insufficient validation of <q>vnd.libreoffice.command</q>
    URI schemes. LibreOffice supports Office URI Schemes to enable browser
    integration of LibreOffice with MS SharePoint server. An additional
    scheme <q>vnd.libreoffice.command</q> specific to LibreOffice was added. In
    the affected versions of LibreOffice links using that scheme could be
    constructed to call internal macros with arbitrary arguments. Which
    when clicked on, or activated by document events, could result in
    arbitrary script execution without warning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26305">CVE-2022-26305</a>

    <p>Compare authors using Thumbprint
    An Improper Certificate Validation vulnerability in LibreOffice
    existed where determining if a macro was signed by a trusted author
    was done by only matching the serial number and issuer string of the
    used certificate with that of a trusted certificate. This is not
    sufficient to verify that the macro was actually signed with the
    certificate. An adversary could therefore create an arbitrary
    certificate with a serial number and an issuer string identical to a
    trusted certificate which LibreOffice would present as belonging to
    the trusted author, potentially leading to the user to execute
    arbitrary code contained in macros improperly trusted.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26306">CVE-2022-26306</a>

    <p>LibreOffice supports the storage of passwords for
    web connections in the user’s configuration database. The stored
    passwords are encrypted with a single master key provided by the
    user. A flaw in LibreOffice existed where the required initialization
    vector for encryption was always the same which weakens the security
    of the encryption making them vulnerable if an attacker has access to
    the user's configuration data</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26307">CVE-2022-26307</a>

    <p>Add Initialization Vectors to password storage.
    LibreOffice supports the storage of passwords for web connections in
    the user’s configuration database. The stored passwords are encrypted
    with a single master key provided by the user. A flaw in LibreOffice
    existed where master key was poorly encoded resulting in weakening its
    entropy from 128 to 43 bits making the stored passwords vulerable to a
    brute force attack if an attacker has access to the users stored
    config.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1:6.1.5-3+deb10u8.</p>

<p>We recommend that you upgrade your libreoffice packages.</p>

<p>For the detailed security status of libreoffice please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libreoffice">https://security-tracker.debian.org/tracker/libreoffice</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3368.data"
# $Id: $

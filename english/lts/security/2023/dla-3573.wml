<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities were found in frr, the FRRouting suite
of internet protocols. Maliciously constructed Border Gateway Protocol
(BGP) packages or corrupted tunnel attributes may cause a denial of service
(application crash) which could be exploited by a remote attacker.</p>

<p>For Debian 10 buster, these problems have been fixed in version
7.5.1-1.1+deb10u1.</p>

<p>We recommend that you upgrade your frr packages.</p>

<p>For the detailed security status of frr please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/frr">https://security-tracker.debian.org/tracker/frr</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3573.data"
# $Id: $

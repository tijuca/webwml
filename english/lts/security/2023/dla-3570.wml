<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A buffer overflow in parsing WebP images may result in the execution of
arbitrary code.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.6.1-2+deb10u3.</p>

<p>We recommend that you upgrade your libwebp packages.</p>

<p>For the detailed security status of libwebp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libwebp">https://security-tracker.debian.org/tracker/libwebp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3570.data"
# $Id: $

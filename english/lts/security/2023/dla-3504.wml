<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple multiple vulnerabilities were discovered in plugins for the
GStreamer media framework and its codecs and demuxers, which may result in
denial of service or potentially the execution of arbitrary code if a
malformed media file is opened.</p>


<p>For Debian 10 buster, this problem has been fixed in version
1.14.4-2+deb10u2.</p>

<p>We recommend that you upgrade your gst-plugins-base1.0 packages.</p>

<p>For the detailed security status of gst-plugins-base1.0 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gst-plugins-base1.0">https://security-tracker.debian.org/tracker/gst-plugins-base1.0</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3504.data"
# $Id: $

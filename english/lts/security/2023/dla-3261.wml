<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential null pointer dereference
vulnerability in libetpan, an low-level library for handling email.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4121">CVE-2022-4121</a>

    <p>Null pointer dereference in mailimap_mailbox_data_status_free in low-level/imap/mailimap_types.c</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1.9.3-2+deb10u2.</p>

<p>We recommend that you upgrade your libetpan packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3261.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in aom, the AV1
Video Codec Library. Buffer overflows, use-after-free and NULL pointer
dereferences may cause a denial of service or other unspecified impact if a
malformed multimedia file is processed.</p>

<p>For Debian 10 buster, these problems have been fixed in version
1.0.0-3+deb10u1.</p>

<p>We recommend that you upgrade your aom packages.</p>

<p>For the detailed security status of aom please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/aom">https://security-tracker.debian.org/tracker/aom</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3556.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in QEMU, a fast processor
emulator, which could result in denial of service, information leak,
or potentially the execution of arbitrary code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14394">CVE-2020-14394</a>

    <p>An infinite loop flaw was found in the USB xHCI controller
    emulation of QEMU while computing the length of the Transfer
    Request Block (TRB) Ring. This flaw allows a privileged guest user
    to hang the QEMU process on the host, resulting in a denial of
    service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17380">CVE-2020-17380</a>/<a href="https://security-tracker.debian.org/tracker/CVE-2021-3409">CVE-2021-3409</a>

    <p>A heap-based buffer overflow was found in QEMU in the SDHCI device
    emulation support. It could occur while doing a multi block SDMA
    transfer via the sdhci_sdma_transfer_multi_blocks() routine in
    hw/sd/sdhci.c. A guest user or process could use this flaw to
    crash the QEMU process on the host, resulting in a denial of
    service condition, or potentially execute arbitrary code with
    privileges of the QEMU process on the host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29130">CVE-2020-29130</a>

    <p>slirp.c has a buffer over-read because it tries to read a certain
    amount of header data even if that exceeds the total packet
    length.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3592">CVE-2021-3592</a>

    <p>An invalid pointer initialization issue was found in the SLiRP
    networking implementation of QEMU. The flaw exists in the
    bootp_input() function and could occur while processing a udp
    packet that is smaller than the size of the <q>bootp_t</q> structure. A
    malicious guest could use this flaw to leak 10 bytes of
    uninitialized heap memory from the host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3593">CVE-2021-3593</a>

    <p>An invalid pointer initialization issue was found in the SLiRP
    networking implementation of QEMU. The flaw exists in the
    udp6_input() function and could occur while processing a udp
    packet that is smaller than the size of the <q>udphdr</q>
    structure. This issue may lead to out-of-bounds read access or
    indirect host memory disclosure to the guest.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3594">CVE-2021-3594</a>

    <p>An invalid pointer initialization issue was found
    in the SLiRP networking implementation of QEMU. The flaw exists in
    the udp_input() function and could occur while processing a udp
    packet that is smaller than the size of the <q>udphdr</q>
    structure. This issue may lead to out-of-bounds read access or
    indirect host memory disclosure to the guest.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3595">CVE-2021-3595</a>

    <p>An invalid pointer initialization issue was found in the SLiRP
    networking implementation of QEMU. The flaw exists in the
    tftp_input() function and could occur while processing a udp
    packet that is smaller than the size of the <q>tftp_t</q>
    structure. This issue may lead to out-of-bounds read access or
    indirect host memory disclosure to the guest.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0216">CVE-2022-0216</a>

    <p>A use-after-free vulnerability was found in the LSI53C895A SCSI
    Host Bus Adapter emulation of QEMU. The flaw occurs while
    processing repeated messages to cancel the current SCSI request
    via the lsi_do_msgout function. This flaw allows a malicious
    privileged user within the guest to crash the QEMU process on the
    host, resulting in a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1050">CVE-2022-1050</a>

    <p>A flaw was found in the QEMU implementation of VMWare's
    paravirtual RDMA device. This flaw allows a crafted guest driver
    to execute HW commands when shared buffers are not yet allocated,
    potentially leading to a use-after-free condition.
    Note: PVRDMA is disabled in buster, but this was fixed
    preventively in case this changes in the future.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1:3.1+dfsg-8+deb10u10.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>For the detailed security status of qemu please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qemu">https://security-tracker.debian.org/tracker/qemu</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3362.data"
# $Id: $

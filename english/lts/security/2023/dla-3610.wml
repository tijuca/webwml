<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Security vulnerabilities were found in python-urllib3, an HTTP library
with thread-safe connection pooling for Python, which could lead to
information disclosure or authorization bypass.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25091">CVE-2018-25091</a>

    <p>Yoshida Katsuhiko discovered that the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2018-20060">CVE-2018-20060</a>
    did not cover non-titlecase request headers; for instance
    “<code>authorization</code>” request headers were not removed during
    during cross-origin redirects.
    (Per <a href="https://datatracker.ietf.org/doc/html/rfc7230#section-3.2">RFC7230 sec. 3.2</a> header fields are
    to be treated case-insensitively.)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11236">CVE-2019-11236</a>

    <p>Hanno Böck discovered that an attacker controlling the request
    parameter can inject headers by injecting CR/LF chars.  The issue is
    similar to CPython's <a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11324">CVE-2019-11324</a>

    <p>Christian Heimes discovered that when verifying HTTPS connections
    upon passing an <code>SSLContext</code> to urllib3, system CA certificates are
    loaded into the <code>SSLContext</code> by default in addition to any
    manually-specified CA certificates.
    This causes TLS handshakes that should fail given only the manually
    specified certs to succeed based on system CA certs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26137">CVE-2020-26137</a>

    <p>It was discovered that CRLF injection was possible if the attacker
    controls the HTTP request method, as demonstrated by inserting CR
    and LF control characters in the first argument of <code>putrequest()</code>.
    The issue is similar to urllib's <a href="https://security-tracker.debian.org/tracker/CVE-2020-26116">CVE-2020-26116</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-43804">CVE-2023-43804</a>

    <p>It was discovered that the <code>Cookie</code> request header isn't stripped
    during cross-origin redirects.  It is therefore possible for a user
    specifying a <code>Cookie</code> header to unknowingly leak information via HTTP
    redirects to a different origin (unless the user disables redirects
    explicitly).  The issue is similar to <a href="https://security-tracker.debian.org/tracker/CVE-2018-20060">CVE-2018-20060</a>, but for <code>Cookie</code>
    request header rather than <code>Authorization</code>.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.24.1-1+deb10u1.</p>

<p>We recommend that you upgrade your python-urllib3 packages.</p>

<p>For the detailed security status of python-urllib3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-urllib3">https://security-tracker.debian.org/tracker/python-urllib3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3610.data"
# $Id: $

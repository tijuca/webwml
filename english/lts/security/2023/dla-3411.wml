<define-tag description>LTS database update</define-tag>
<define-tag moreinfo>
<p>This is a routine update of the distro-info-data database for Debian LTS users.</p>

<p>It includes the expected release date for Debian 12, adds Debian 14, adds
Ubuntu 23.10, and some minor updates to EoL dates for Ubuntu releases.</p>

<p>For Debian 10 buster, these issues have been fixed in version 0.41+deb10u6.</p>

<p>We recommend that you upgrade your distro-info-data packages.</p>

<p>For the detailed security status of distro-info-data please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/distro-info-data">https://security-tracker.debian.org/tracker/distro-info-data</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3411.data"
# $Id: $

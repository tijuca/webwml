<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in ncurses, a collection of shared libraries for
terminal handling.
This issue is about an out-of-bounds read in convert_strings in the
terminfo library.</p>


<p>For Debian 10 buster, this problem has been fixed in version
6.1+20181013-2+deb10u3.</p>

<p>We recommend that you upgrade your ncurses packages.</p>

<p>For the detailed security status of ncurses please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ncurses">https://security-tracker.debian.org/tracker/ncurses</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LT">https://wiki.debian.org/LT</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3167.data"
# $Id: $

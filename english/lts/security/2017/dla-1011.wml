<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Todd Miller's sudo version 1.8.20p1 and earlier is vulnerable to an
input validation (embedded newlines) in the get_process_ttyname()
function resulting in information disclosure and command execution.</p>

<p>The previous announcement (DLA-970-1) was about a similar security
issue (<a href="https://security-tracker.debian.org/tracker/CVE-2017-1000367">CVE-2017-1000367</a>) which wasn't completely fixed.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.8.5p2-1+nmu3+deb7u4.</p>

<p>We recommend that you upgrade your sudo packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1011.data"
# $Id: $

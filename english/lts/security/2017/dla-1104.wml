<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that podbeuter, the podcast fetcher in newsbeuter, a
text-mode RSS feed reader, did not properly escape the name of the media
enclosure (the podcast file), allowing a remote attacker to run an
arbitrary shell command on the client machine. This is only exploitable
if the file is also played in podbeuter.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.5-2+deb7u3.</p>

<p>We recommend that you upgrade your newsbeuter packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1104.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Apache PDFBox did not properly initialize the XML parsers, which
allows context-dependent attackers to conduct XML External Entity
(XXE) attacks via a crafted PDF. This may lead to the disclosure of
confidential data, denial of service, server side request forgery,
port scanning from the perspective of the machine where the parser is
located, and other system impacts.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:1.7.0+dfsg-4+deb7u1.</p>

<p>We recommend that you upgrade your libpdfbox-java packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-505.data"
# $Id: $

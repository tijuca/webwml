<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that it was possible to bypass SASL negotiation
isComplete validation in libthrift-java, Java language support for the
Apache Thrift software framework. An assert used to determine if the
SASL handshake had successfully completed could be disabled in
production settings making the validation incomplete.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.9.1-2+deb8u1.</p>

<p>We recommend that you upgrade your libthrift-java packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1662.data"
# $Id: $

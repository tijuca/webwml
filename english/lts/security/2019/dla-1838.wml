<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several minor issues have been fixed in mupdf, a lightweight PDF viewer
tailored for display of high quality anti-aliased graphics.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5686">CVE-2018-5686</a>

    <p>In MuPDF, there was an infinite loop vulnerability and application
    hang in the pdf_parse_array function (pdf/pdf-parse.c) because EOF
    not having been considered. Remote attackers could leverage this
    vulnerability to cause a denial of service via a crafted PDF file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6130">CVE-2019-6130</a>

    <p>MuPDF had a SEGV in the function fz_load_page of the fitz/document.c
    file, as demonstrated by mutool. This was related to page-number
    mishandling in cbz/mucbz.c, cbz/muimg.c, and svg/svg-doc.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6192">CVE-2018-6192</a>

    <p>In MuPDF, the pdf_read_new_xref function in pdf/pdf-xref.c allowed
    remote attackers to cause a denial of service (segmentation violation
    and application crash) via a crafted PDF file.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.5-1+deb8u6.</p>

<p>We recommend that you upgrade your mupdf packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1838.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in imagemagick, an image processing
toolkit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12974">CVE-2019-12974</a>

    <p>NULL pointer dereference in ReadPANGOImage and ReadVIDImage (coders/pango.c
    and coders/vid.c). This vulnerability might be leveraged by remote attackers
    to cause denial of service via crafted image data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13135">CVE-2019-13135</a>

    <p>Multiple use of uninitialized values in ReadCUTImage, UnpackWPG2Raster and
    UnpackWPGRaster (coders/wpg.c and coders/cut.c). These vulnerabilities might
    be leveraged by remote attackers to cause denial of service or unauthorized
    disclosure or modification of information via crafted image data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13295">CVE-2019-13295</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-13297">CVE-2019-13297</a>

    <p>Multiple heap buffer over-reads in AdaptiveThresholdImage
    (magick/threshold.c). These vulnerabilities might be leveraged by remote
    attackers to cause denial of service or unauthorized disclosure or
    modification of information via crafted image data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13304">CVE-2019-13304</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-13305">CVE-2019-13305</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-13306">CVE-2019-13306</a>

    <p>Multiple stack buffer overflows in WritePNMImage (coders/pnm.c), leading to
    stack buffer over write up to ten bytes. Remote attackers might leverage
    these flaws to potentially perform code execution or denial of service.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
8:6.8.9.9-5+deb8u17.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1888.data"
# $Id: $
